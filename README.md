# Proyecto de Software 2019 - Grupo 15
## Orquesta Escuela de Berisso

1.  Clone project

2.  Install virtual env into folder 
    > `$ pip install virtualenv` 

3.  Create virtual env into folder 
    > `$ virtualenv venv` 

4.  Active virtual env into folder 
    > `source venv/bin/activate` 

5.  Install all dependencies 
    > `pip3 install -r requirements.txt` 
    or
    > `pip install -r requirements.txt` 


6.  Configure enviroment variable 
    > `export FLASK_ENV=development` 

7.  Configure app run file 
    > `export FLASK_APP=run.py` 

8.  Run the app 
    > `flask run` 

9.  Move to **flaskps/client** folder and run command 
    > `sudo npm install` 

10. In addition to this you must load the database dump that is located in the folder **flaskps/database/dump**  (Using mysql worbench or similar)
    > *  Load structure dump 
    > *  Load data dump


11. Create **developmnet.py** file for the flask app in **flaskps/config** and assign this data 

    > *   DB_HOST = 'your db host'
    > *   DB_USER = 'your db user'
    > *   DB_PASS = 'your db pass'
    > *   DB_NAME = 'db name'
    > *   SECRET_KEY = '677275706f3135'
    > *   WTF_CSRF_ENABLED = False


12. Move to **flaskps/client** and run command 
    > `npm run serve` 
    
13. Access the site from ip and port specified from the client 
    > Ej. [http://localhost:8080](http://localhost:8080

14. Request access credentials to system administrators or use the following account as a test
    > Email: roxaneseel@gmail.com
    > Password: rs123
