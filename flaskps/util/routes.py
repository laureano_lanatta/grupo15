from flaskps import app
from flaskps.controllers import home
from flaskps.controllers import auth
from flaskps.controllers import configuration
from flaskps.controllers import role
from flaskps.controllers import user
from flaskps.controllers import administrator
from flaskps.controllers import preceptor
from flaskps.controllers import teacher
from flaskps.controllers import mail_sender
from flaskps.controllers import student
from flaskps.controllers import guardian
from flaskps.controllers import instrument
from flaskps.controllers import instrument_type
from flaskps.controllers import center
from flaskps.controllers import period
from flaskps.controllers import workshop
from flaskps.controllers import enrollment
from flaskps.controllers import school
from flaskps.controllers import level
from flaskps.controllers import neighborhood
from flaskps.controllers import gender
from flaskps.controllers import about
from flaskps.controllers import weekday
from flaskps.controllers import event
from flaskps.controllers import venue
from flaskps.controllers import attendance
from flaskps.controllers import attendee
from flaskps.controllers import loan
# Home
app.add_url_rule('/', 'home_index', home.index)

# Auth
app.add_url_rule('/auth', 'auth_authenticate', auth.authenticate, methods=['POST'])
app.add_url_rule('/logout', 'auth_logout', auth.logout)
app.add_url_rule('/authorize/<endpoint>', 'auth_authorize', auth.authorize)

# Config
app.add_url_rule('/configuration', 'configuration_index', configuration.index)
app.add_url_rule('/configuration/publicInfo', 'configuration_public_info_index', configuration.public_info)
app.add_url_rule('/configuration/pagination', 'configuration_pagination_index', configuration.pagination)
app.add_url_rule('/configuration/maintenanceMode', 'configuration_maintenance_mode_index', configuration.maintenance_mode)
# app.add_url_rule("/configuration/roles", 'configuration_roles',role.roles)
app.add_url_rule('/configuration/createRole', 'configuration_create_role', role.create_role,methods=['POST'])
app.add_url_rule('/configuration/updateRole', 'configuration_update_role', role.update_role,methods=['POST'])
app.add_url_rule('/configuration/deleteRole', 'configuration_delete_role', role.delete_role,methods=['POST'])
app.add_url_rule('/configuration/showRoles', 'configuration_show_roles', role.show_roles)
app.add_url_rule('/configuration/showPermissions', 'configuration_show_permissions', role.show_permissions)
app.add_url_rule('/configuration/showRolesPermissions', 'configuration_show_roles_permissions', role.show_all)
app.add_url_rule('/configuration/createPermission', 'configuration_create_permission', role.create_permission,methods=['POST'])
app.add_url_rule('/configuration/editPermission', 'configuration_update_permission', role.update_permission,methods=['POST'])
app.add_url_rule('/configuration/deletePermission', 'configuration_delete_permission', role.delete_permission,methods=['POST'])
app.add_url_rule('/configuration/assignPermissions', 'configuration_assign_permissions', role.assign_permissions,methods=['POST'])
app.add_url_rule('/configuration/updatePagination', 'configuration_update_pagination', configuration.update_pagination, methods=['POST'])
app.add_url_rule('/configuration/updatePublicInfo', 'configuration_update_public_info', configuration.update_public_info, methods=['POST'])
app.add_url_rule('/configuration/toggleMaintenanceMode', 'configuration_toggle_maintenance_mode', configuration.toggle_maintenance_mode, methods=['POST'])

################################################## USER ROUTES ############################################################

# User list
app.add_url_rule('/user','user_index', user.index)
app.add_url_rule('/user_role_cant','user_role_cant',user.get_cant_roles)
app.add_url_rule('/user_confirm_cant','user_confirm_cant',user.get_cant_confirmed)
app.add_url_rule('/user_bloqued_cant','user_bloqued_cant',user.get_cant_bloqued)

# User Profile
app.add_url_rule('/user/<id>','user_profile', user.get_user_profile, methods=['GET'])   
app.add_url_rule('/user/<id>','user_update_info', user.user_update, methods=['PUT'])    
app.add_url_rule('/user/<id>','user_delete', user.user_delete, methods=['DELETE'])    

app.add_url_rule('/user/<id>/block','user_get_block', user.get_bloqued_value, methods=['GET'])    
app.add_url_rule('/user/<id>/block','user_block', user.set_bloqued_value, methods=['PUT'])     

app.add_url_rule('/user/<id>/roles','user_get_roles', user.get_user_roles, methods=['GET'])        
app.add_url_rule('/user/<id>/roles','user_update_role', user.set_user_roles, methods=['PUT']) 

app.add_url_rule('/user/exist/<id>','user_exist', user.exist, methods=['GET'])   

# User New 
app.add_url_rule('/user/activate','change_password', user.activate_account, methods=['POST'])
app.add_url_rule('/user/new','user_new', user.up, methods=['POST'])

##########################################################################################################################
################################################ TEACHER ROUTES ##############################################################

# Teacher list
app.add_url_rule('/teacher','teacher_index', teacher.index)

# Teacher Profile
app.add_url_rule('/teacher/<id>','teacher_profile', teacher.get_teacher_profile, methods=['GET'])         
app.add_url_rule('/teacher/<id>','teacher_update_info',teacher.teacher_update, methods=['PUT']) 

# Teacher Enrolled
app.add_url_rule('/teacher/enrolled/<id_enrollment>','teacher_enrolled', teacher.enrolled)
app.add_url_rule('/teacher/notEnrolled/<id_enrollment>','teacher_not_enrolled', teacher.notEnrolled)

##########################################################################################################################
################################################ STUDENT ROUTES ##############################################################

# Student list
app.add_url_rule('/student','student_index', student.index)
app.add_url_rule('/student','student_index_small', student.index_small)

# Student Profile
app.add_url_rule('/student/<id>','student_profile', student.get_student_profile, methods=['GET'])         
app.add_url_rule('/student/<id>','student_update_info',student.student_update, methods=['PUT']) 
app.add_url_rule('/student/<id>','student_delete', student.student_delete, methods=['DELETE'])     

app.add_url_rule('/student/exist/<id>','student_exist', student.exist, methods=['GET'])     

# Student New 
app.add_url_rule('/student/new','student_new', student.up, methods=['POST'])

# Student Enrolled
app.add_url_rule('/student/enrolled/<id_enrollment>','student_enrolled', student.enrolled)
app.add_url_rule('/student/notEnrolled/<id_enrollment>','student_not_enrolled', student.notEnrolled)
app.add_url_rule('/student/enrolled_attendance/<id_enrollment>','student_enrolled_attendance', student.enrolled_attendance)

##########################################################################################################################
################################################ GUARDIAN ROUTES ##############################################################

# Guardian list
app.add_url_rule('/student/guardian/<id>','guardian_index', guardian.get_guardian_by_student, methods=['GET'])     

# Guardian Profile
app.add_url_rule('/guardian/<id>','guardian_profile', guardian.get_profile, methods=['GET'])
app.add_url_rule('/guardian/<id>','guardian_update_info', guardian.guardian_update, methods=['PUT']) 

app.add_url_rule('/guardian/exist/<id>','guardian_exist', guardian.exist, methods=['GET'])     

# Guardian New 
app.add_url_rule('/guardian/new','guardian_new', guardian.up, methods=['POST'])


##########################################################################################################################
################################################ CENTER ROUTES ##############################################################

# Center list
app.add_url_rule('/center', 'center_index', center.index)

# Center Profile
app.add_url_rule('/center/<id>','center_profile', center.get_center_profile, methods=['GET'])
app.add_url_rule('/center/<id>','center_update_info', center.center_update, methods=['PUT']) 
app.add_url_rule('/center/<id>','center_delete', center.center_delete, methods=['DELETE'])

app.add_url_rule('/center/exist/<id>','center_exist', center.exist, methods=['GET'])     

# Center New 
app.add_url_rule('/center/new','center_new', center.up, methods=['POST'])

##########################################################################################################################
################################################ VARIOUS ROUTES ##############################################################

#Neiborhood
app.add_url_rule('/neighborhood','get_neighborhood',neighborhood.get_all,methods=['GET']) 

#School
app.add_url_rule('/school','get_school',school.get_all,methods=['GET'])    

#Level
app.add_url_rule('/level','get_level',level.get_all,methods=['GET'])
app.add_url_rule('/level_cant','level_cant',level.get_cant_level)

#Gender
app.add_url_rule('/gender','get_gender',gender.get_all,methods=['GET'])

# Send message
app.add_url_rule('/check','check_link', mail_sender.check_link, methods=['POST'])

##########################################################################################################################
################################################ PRECEPTOR ROUTES ##############################################################

# Preceptor list
app.add_url_rule('/preceptor', 'preceptor_index', preceptor.index)

##########################################################################################################################
################################################ ADMIN ROUTES ##############################################################

# Administrator list
app.add_url_rule('/administrator', 'administrator_index', administrator.index)

##########################################################################################################################

# Enrollment
app.add_url_rule('/enrollment', 'enrollment_index', enrollment.index)
app.add_url_rule('/enrollment/<id>', 'enrollment_show', enrollment.get_by_id)
app.add_url_rule('/enrolled/student/<id_student>', 'enrolled_student_index', enrollment.get_by_student_id)
app.add_url_rule('/enrolled/teacher/<id_teacher>', 'enrolled_teacher_index', enrollment.get_by_teacher_id)
app.add_url_rule('/enrollment/<id_period>/<id_workshop>/<id_center>/<id_weekday>', 'enrollment_search', enrollment.get_id_by_period_workshop_center_weekday)
app.add_url_rule('/enrollment/teacher/<id_enrollment>/<id_teacher>', 'enrollment_create_teacher', enrollment.enroll_teacher, methods=['POST'])
app.add_url_rule('/enrollment/student/<id_enrollment>/<id_student>', 'enrollment_create_student', enrollment.enroll_student, methods=['POST'])
app.add_url_rule('/enrollment/new', 'course_new', enrollment.create,methods=['POST'])
app.add_url_rule('/course/exist/<id>', 'course_exist', enrollment.exist,methods=['GET'])
app.add_url_rule('/course/course_delete', 'course_delete', enrollment.delete,methods=['POST'])
app.add_url_rule('/course/<id>', 'course_profile', enrollment.get_by_id_only,methods=['GET'])
app.add_url_rule('/course_update_info', 'course_update_info', enrollment.update,methods=['POST'])
app.add_url_rule('/enrollment/<id_enrollment>/<id_teacher>', 'enrollment_exist_teacher', enrollment.exist_teacher, methods=['GET'])

# Workshop
app.add_url_rule('/newWorkshop','new_workshop', workshop.create, methods=['POST'])
app.add_url_rule('/workshop','workshop_index', workshop.index)
app.add_url_rule('/deleteWorkshop/<id>','workshop_delete',workshop.delete, methods=['POST'])
app.add_url_rule('/editWorkshop/<id>','workshop_edit', workshop.edit, methods=['GET'])
app.add_url_rule('/editFinalWorkShop', 'workshop_finalEdit', workshop.finalEdit, methods=['POST'])

# About
app.add_url_rule('/getPublicInfo', 'about_getInfo', about.getInfo, methods=['GET'])

# Period
app.add_url_rule('/period', 'period_index', period.index)
app.add_url_rule('/period/new', 'period_create', period.create,methods=['POST'])
app.add_url_rule('/period/<id>','period_profile', period.get_profile, methods=['GET'])
app.add_url_rule('/period/exist/<id>','period_exist', period.exist, methods=['GET'])     
app.add_url_rule('/period_update_info', 'period_update_info', period.update, methods=['POST']) 
app.add_url_rule('/period/period_delete', 'period_delete', period.delete,methods=['POST'])
# app.add_url_rule('/period/enrollment', 'period_enrollment', period.get_enrollments)
# app.add_url_rule('/period/workshop', 'period_workshop', period.get_workshop)
# app.add_url_rule('/period/assignWorkshop', 'period_assign_workshop', period.assign_workshop,methods=['POST'])

# Center
app.add_url_rule('/center', 'center_index', center.index)
app.add_url_rule('/center/new','center_new', center.up, methods=['POST'])
app.add_url_rule('/center/<id>','center_profile', center.get_center_profile, methods=['GET'])
app.add_url_rule('/center/exist/<id>','center_exist', center.exist, methods=['GET'])     
app.add_url_rule('/center_update_info', 'center_update_info', center.center_update, methods=['POST']) 
app.add_url_rule('/center/center_delete', 'center_delete', center.center_delete, methods=['POST'])        
        

# Weekday
app.add_url_rule('/weekday', 'weekday_index', weekday.index)

#Instrument
app.add_url_rule('/instruments', 'instrument_index', instrument.index)
app.add_url_rule('/instrument/<id>','instrument_profile', instrument.get_instrument_profile, methods=['GET'])
app.add_url_rule('/instrument/exist/<id>','instrument_exist', instrument.exist, methods=['GET'])     
app.add_url_rule('/instrument_update_info', 'instrument_update_info', instrument.update,methods=['POST'])
app.add_url_rule('/instrument/instrument_delete', 'instrument_delete', instrument.delete,methods=['POST'])
app.add_url_rule('/instrument/new', 'instrument_new', instrument.create,methods=['POST'])
app.add_url_rule('/instrument_type', 'instrument_type_index', instrument_type.index)
app.add_url_rule('/instrument_type/new', 'instrument_type_new', instrument_type.create,methods=['POST'])
app.add_url_rule('/instrument_type/instrument_type_delete', 'instrument_type_delete', instrument_type.delete,methods=['POST'])

# Event
app.add_url_rule('/event', 'event_index', event.index)
app.add_url_rule('/event/create', 'event_create', event.create, methods=['POST'])
app.add_url_rule('/event/<id>', 'event_show', event.show)
app.add_url_rule('/event/<id>/delete', 'event_delete', event.delete, methods=['DELETE'])
app.add_url_rule('/event/<id>/invite', 'event_invite', event.invite, methods=['POST'])

# Venue
app.add_url_rule('/venue', 'venue_index', venue.index)
app.add_url_rule('/venue/create', 'venue_create', venue.create, methods=['POST'])

# Attendance
app.add_url_rule("/attendance/<id>","attendance_index",attendance.index,methods=['GET'])
app.add_url_rule("/attendance/new","attendance_new",attendance.create,methods=['POST'])
#Attendee
app.add_url_rule('/attendee/<event_id>', 'attendee_event', attendee.by_event)

# Loan
app.add_url_rule('/loan', 'loan_index', loan.index, methods=['GET'])
app.add_url_rule('/newLoan', 'new_loan', loan.new, methods=['GET', 'POST'])