from flask import Response, jsonify, request
from flaskps.models import enrollment
from flaskps.models import period
from flaskps.models import workshop
from flaskps.models import center
from flaskps.models import weekday
from flaskps.classes.forms.enrollment import EnrollmentForm
from datetime import datetime


def index():
    response = [{
        'id': enrollment.Enrollment.id,
        'workshop': {
            'id': enrollment.Workshop.id,
            'name': enrollment.Workshop.name
        },
        'period': {
            'id': enrollment.Period.id,
            'start': enrollment.Period.start.strftime("%d/%m/%Y"),
            'end': enrollment.Period.end.strftime("%d/%m/%Y"),
            'semester': enrollment.Period.semester
        },
        'center': {
            'id': enrollment.Center.id,
            'name': enrollment.Center.name
        },
        'weekday': {
            'id': enrollment.Weekday.id,
            'name': enrollment.Weekday.name
        },
        'hour': enrollment.Enrollment.hour.strftime("%H:%M %p")
    } for enrollment in enrollment.get_all()]
    return jsonify(response)


def get_by_id(id):
    e = enrollment.get_by_id(id)
    response = {
        'id': e.Enrollment.id,
        'workshop': {
            'id': e.Workshop.id,
            'name': e.Workshop.name
        },
        'period': {
            'id': e.Period.id,
            'start': e.Period.start.strftime("%d/%m/%Y"),
            'end': e.Period.end.strftime("%d/%m/%Y"),
            'semester': e.Period.semester
        },
        'center': {
            'id': e.Center.id,
            'name': e.Center.name
        },
        'weekday': {
            'id': e.Weekday.id,
            'name': e.Weekday.name
        },
        'hour': e.Enrollment.hour.strftime("%H:%M %p")
    }
    return jsonify(response)


def get_by_id_only(id):
    e = enrollment.get_by_id_only(id)
    response = {
        'id': e.id,
        'workshop': e.id_workshop,
        'period': e.id_period,
        'center': e.id_center,
        'weekday': e.id_weekday,
        "hour": e.hour.strftime("%H:%M")
    }
    return jsonify(response)


def get_id_by_period_workshop_center_weekday(id_period, id_workshop, id_center, id_weekday):
    return {'id': enrollment.get_id_by_period_workshop_center_weekday(id_period, id_workshop, id_center, id_weekday).id}


def get_by_student_id(id_student):
    response = [{
        'id': enrollment.Enrollment.id,
        'workshop': {
            'id': enrollment.Workshop.id,
            'name': enrollment.Workshop.name
        },
        'period': {
            'id': enrollment.Period.id,
            'start': enrollment.Period.start.strftime("%d/%m/%Y"),
            'end': enrollment.Period.end.strftime("%d/%m/%Y"),
            'semester': enrollment.Period.semester
        },
        'center': {
            'id': enrollment.Center.id,
            'name': enrollment.Center.name
        },
        'weekday': {
            'id': enrollment.Weekday.id,
            'name': enrollment.Weekday.name
        }
    } for enrollment in enrollment.get_by_student_id(id_student)]
    return jsonify(response)


def get_by_teacher_id(id_teacher):
    response = [{
        'id': enrollment.Enrollment.id,
        'workshop': {
            'id': enrollment.Workshop.id,
            'name': enrollment.Workshop.name
        },
        'period': {
            'id': enrollment.Period.id,
            'start': enrollment.Period.start.strftime("%d/%m/%Y"),
            'end': enrollment.Period.end.strftime("%d/%m/%Y"),
            'semester': enrollment.Period.semester
        },
        'center': {
            'id': enrollment.Center.id,
            'name': enrollment.Center.name
        },
        'weekday': {
            'id': enrollment.Weekday.id,
            'name': enrollment.Weekday.name
        }
    } for enrollment in enrollment.get_by_teacher_id(id_teacher)]
    return jsonify(response)


def enroll_teacher(id_enrollment, id_teacher):
    enrollment.enroll_teacher(id_enrollment, id_teacher)
    return Response(status=200)


def enroll_student(id_enrollment, id_student):
    enrollment.enroll_student(id_enrollment, id_student)
    return Response(status=200)


def create():
    form = EnrollmentForm()
    if form.validate_on_submit():
        if period.check_exist_by_id(request.form['period']):
            if workshop.check_exist_by_id(request.form['workshop']):
                if center.check_exist_by_id(request.form['center']):
                    if weekday.check_exist_by_id(request.form['weekday']):
                        if not enrollment.check_exist_with(request.form['period'], request.form['workshop'], request.form['center'], request.form['weekday']):
                            hour = datetime.strptime(
                                request.form['hour'], '%H:%M').time()
                            lastId = enrollment.create(
                                request.form['period'], request.form['workshop'], request.form['center'], request.form['weekday'], hour)
                            response = jsonify(lastId)
                            response.status_code = 200
                            return response
                        else:
                            response = jsonify(["weekday"])
                            response.status_code = 400
                            return response
                    else:
                        response = jsonify(["weekday"])
                        response.status_code = 400
                        return response
                else:
                    response = jsonify(["center"])
                    response.status_code = 400
                    return response
            else:
                response = jsonify(["workshop"])
                response.status_code = 400
                return response
        else:
            response = jsonify(["period"])
            response.status_code = 400
            return response
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def delete():
    if enrollment.check_exist_by_id(request.form['id']):
        course = enrollment.get_by_id(request.form['id'])
        today = datetime.now().date()
        if(course.Period.end < today):
            enrollment.delete(request.form['id'])
            return Response(status=200)
        else:
            response = jsonify(
                "No se puede eliminar el curso. El mismo tadavia no finalizo")
            response.status_code = 400
            return response
    else:
        response = jsonify("El curso que desea eliminar no existe")
        response.status_code = 400
        return response


def update():
    form = EnrollmentForm()
    if form.validate_on_submit():
        if enrollment.check_exist_by_id(request.form["id"]):
            if workshop.check_exist_by_id(request.form['workshop']):
                if center.check_exist_by_id(request.form['center']):
                    hour = datetime.strptime(
                        request.form['hour'], '%H:%M').time()
                    enrollment.update(request.form["id"],request.form["workshop"],request.form["center"],hour)
                    return Response(status=200)
                else:
                    response = jsonify(["center"])
                    response.status_code = 400
                    return response
            else:
                response = jsonify(["workshop"])
                response.status_code = 400
                return response
        else:
            response = jsonify(["id"])
            response.status_code = 400
            return response
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def exist(id):
    if (enrollment.check_exist_by_id(id)):
        response = {'value': 'True'}
    else:
        response = {'value': 'False'}
    return jsonify(response)

def exist_teacher(id_enrollment,id_teacher):
    if (enrollment.check_exist_by_enrollment_teacher(id_enrollment,id_teacher)):
        response = {'value': True}
    else:
        response = {'value': False}
    return jsonify(response)
