from flask import request, Response, jsonify
from flaskps.models import instrument_type
from flaskps.classes.forms.instrument_type import InstrumentTypeForm


def index():
    response = [{
        'id': ins_type.id,
        'name': ins_type.name,
    } for ins_type in instrument_type.get_all()]
    return jsonify(response)


def create():
    form = InstrumentTypeForm()
    if form.validate_on_submit():
        if not instrument_type.check_exist_by_name(request.form['name']):
            lastId = instrument_type.create(request.form['name'])
            response = jsonify(lastId)
            response.status_code = 200
            return response
        else:
            response = jsonify(["name"])
            response.status_code = 400
            return response
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def delete():
    if instrument_type.check_exist_by_id(request.form['id']):
        if instrument_type.check_exist_instrument_with_type(request.form['id']):
            response = jsonify(
                "Existen instrumentos que utilizan este tipo")
            response.status_code = 400
            return response
        else:
            instrument_type.delete(request.form['id'])
            return Response(status=200)
    else:
        response = jsonify(
            "El tipo de instrumento que desea eliminar no existe")
        response.status_code = 400
        return response
