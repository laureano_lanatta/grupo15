import requests
from flask import request, Response, jsonify


def headers():
    return {'Authorization': 'Bearer TIONJAE3O3WSGF6E5SPZ'}


def by_event(event_id):
    response = requests.get(f'https://www.eventbriteapi.com/v3/events/{event_id}/attendees/', headers=headers())
    acumulator = {}
    for attendee in response.json()["attendees"]:
        if attendee["profile"]["email"] in acumulator:
            acumulator[attendee["profile"]["email"]] += 1
        else:
            acumulator[attendee["profile"]["email"]] = 1
    attendees_set = list({attendee["profile"]["email"]:attendee for attendee in response.json()["attendees"]}.values())
    for attendee in attendees_set:
        attendee["amount"] = str(acumulator[attendee["profile"]["email"]])
    return jsonify(attendees_set)