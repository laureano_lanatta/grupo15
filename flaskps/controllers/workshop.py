from flask import request, Response, jsonify
from flaskps.models.workshop import Workshop
from flaskps.models import workshop

def create():
    data = str(request.form['inputName'])
    if (workshop.validate(data)):
        return Response(status=400)
    else:
        workshop.create_workshop(data)
        return Response(status=200)


def index():
    response = [{
        'id': workshop.id,
        'name': workshop.name
    } for workshop in workshop.get_all()]
    return jsonify(response)


def delete(id):
    if workshop.delete_workshop(id):
        return Response(status=200)
    else: 
        return Response(status=400)


def edit(id):
    data = workshop.get_by_id(id)
    response = {
        'id' : data.id,
        'name' : data.name
    }
    return jsonify(response)


def finalEdit():
    print("llegue a finalEdit")
    id_workshop = int(request.form['id'])
    name = str(request.form['inputName'])
    workshop.updateWorkshop(id_workshop, name)
    return Response(status=200)

