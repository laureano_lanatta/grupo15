from flask import jsonify
from flaskps.models import weekday


def index():
    response = [{
        'id': weekday.id,
        'name': weekday.name
    } for weekday in weekday.get_all()]
    return jsonify(response)
