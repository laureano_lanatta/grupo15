from flaskps.models import school
from flask import jsonify


def get_all():
    response = [{
        'id': school.id,
        'name':school.name
    } for school in school.get_all()]
    return jsonify(response)