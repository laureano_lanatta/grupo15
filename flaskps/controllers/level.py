from flaskps.models import level
from flask import jsonify


def get_all():
    response = [{
        'id': level.id,
        'name':level.name
    } for level in level.get_all()]
    return jsonify(response)


def get_cant_level():    
    response = [{
        'name':leve.name,
        'cant': level.get_cant_level(leve.id),

    } for leve in level.get_all()]         
    return jsonify(response)