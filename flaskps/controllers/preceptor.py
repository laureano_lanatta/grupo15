from flask import jsonify
from flaskps.models import preceptor


def index():
    response = [{
        'id': preceptor.id,
        'username': preceptor.username,
        'email': preceptor.email,
        'first_name': preceptor.first_name,
        'last_name': preceptor.last_name
    } for preceptor in preceptor.get_all()]
    return jsonify(response)
