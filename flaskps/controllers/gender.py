from flaskps.models import gender
from flask import jsonify


def get_all():
    response = [{
        'id': gender.id,
        'name':gender.name
    } for gender in gender.get_all()]   
    return jsonify(response)