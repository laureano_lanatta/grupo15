from flaskps.models import student, school, neighborhood, level, guardian
from flask import request, Response, jsonify
from flaskps.classes.forms.Student import addStudentform, updateStudentform
from datetime import datetime


def index_small():
    response = [{
        'id': student.Student.id,
        'first_name': student.Student.first_name,
        'last_name': student.Student.last_name,
        'level': {
            'id': student.Level.id,
            'name': student.Level.name
        }
    } for student in student.get_all_small()]
    return jsonify(response)


def enrolled(id_enrollment):
    response = [{
        'id': student.Student.id,
        'first_name': student.Student.first_name,
        'last_name': student.Student.last_name,
        'level': {
            'id': student.Level.id,
            'name': student.Level.name
        }
    } for student in student.get_enrolled(id_enrollment)]
    return jsonify(response)

def enrolled_attendance(id_enrollment):
    response =[{
        'id': student["id"],
        'first_name': student["first_name"],
        'last_name':student["last_name"],
        'level':student["level"],
        'count_attendance':student["count_attendance"],
        "total_count":student["total_count"]
    } for student in student.get_enrolled_attendance(id_enrollment)]
    return jsonify(response)


def notEnrolled(id_enrollment):
    enrolled_student_ids = [student.Student.id for student in student.get_enrolled(id_enrollment)]
    response = [{
        'id': student.Student.id,
        'first_name': student.Student.first_name,
        'last_name': student.Student.last_name,
        'level': {
            'id': student.Level.id,
            'name': student.Level.name
        }
    } for student in student.get_all_small() if student.Student.id not in enrolled_student_ids]
    return jsonify(response)


def index():
    response = [{
        'id': student.id,
        'document_number': student.document_number,
        'id_document_type': student.id_document_type,
        'first_name': student.first_name,
        'last_name': student.last_name,
        'phone_number': student.phone_number,
        'id_level': level.get_name_by_id(student.id_level),
        'id_city': student.id_city,
        'birthplace': student.id_birthplace,
        'birthday': student.birthday,
        'id_school': school.get_name_by_id(student.id_school),
        'id_gender': student.id_gender,
        'id_neighborhood': neighborhood.get_name_by_id(student.id_neighborhood),
        'address': student.address
    } for student in student.get_all()]
    return jsonify(response)     


def up():
    form = addStudentform()
    if form.validate_on_submit():
        fn = request.form['first_name']
        ln = request.form['last_name']
        phone = request.form['phone']
        address = request.form['address']
        id_l = request.form['level']
        id_c = request.form['city']
        birthday = request.form['birth']
        birthplace = request.form['birthplace']
        dni = request.form['dni']
        dnitype = request.form['dnitype']
        id_n = request.form['neighborhood']
        sex = request.form['sex']
        school = request.form['school']             

        if str(student.check_exist_by_dni(dni)) == '1':
            return Response(status=501)
        else:    
            student.create(fn, ln, phone, address, id_l, id_c, birthday, dni, id_n, dnitype, school, birthplace, sex)                      
            return Response(status=200)
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def exist(id):  
    if (student.check_exist_by_id(id) == 1): 
        if (guardian.get_count_guardian_by_id_student(id) > 0):       
            response = {'value':'True', 'guardian': 'True' }
        else:
            response = {'value':'True', 'guardian': 'False' }

    else:
        response = {'value':'False', 'guardian': 'False' }
    return jsonify(response)


def get_student_profile(id):
    if (student.check_exist_by_id(id) == 1):
        data = student.get_by_id(id)        
        response = {
        'student_data_first_name': data.first_name,
        'student_data_last_name': data.last_name,
        'student_data_dni': data.document_number,
        'student_data_dnitype': data.id_document_type,
        'student_data_birthday': data.birthday.strftime("%Y-%m-%d"),
        'student_data_birthplace': data.id_birthplace,
        'student_data_address': data.address,
        'student_data_phone': data.phone_number,
        'student_data_id_city': data.id_city,
        'student_data_id_level': data.id_level,        
        'student_data_id_neighborhood': data.id_neighborhood,
        'student_data_id_school': data.id_school,
        'student_data_id_gender': data.id_gender,
        }
        return jsonify(response)
    else:     
        return Response(status=400)    


def student_update(id):
    form = updateStudentform()
    if form.validate_on_submit():
        #vars
        fn = request.form['first_name_input']
        ln = request.form['last_name_input']
        address = request.form['address_input']
        dni = request.form['dni_input']
        dnitype = request.form['dnitype_input']
        city = request.form['city_input']     
        phone = request.form['phone_input']   
        birthday = request.form['birthday_input']
        school = request.form['school_input']
        level = request.form['level_input']
        sex = request.form['sex_input']
        neighborhood = request.form['neighborhood_input']
        birthplace = request.form['birthplace_input']           

        if student.get_dni_by_id(id) != dni:
            if student.check_exist_by_dni(dni) == 1:
                return Response(status=501)
        #update
        student.update_student_info(id, fn, ln, birthday, address, dni, dnitype, phone, city, school, level, neighborhood, birthplace, sex)                            
        return Response(status=200)        
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response    


def student_delete(id):   
    student.delete_student(id) 
    return Response(status=200)  