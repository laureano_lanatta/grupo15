from flask import request, Response, jsonify
from flaskps.models import loan
from datetime import datetime

def index():
    response = [{
        'id_inventory': loan[0].id_inventory_item,
        'inventory_name': loan[2].name,
        'id_student': loan[0].id_student,
        'student_name': loan[1].first_name,
        'started_at' : loan[0].started_at,
        'ended_at' : loan[0].ended_at,
    } for loan in loan.getAll()]
    return jsonify(response)


def new():

    id_inventory = request.form['id_inventory_item']
    id_student = request.form['id_student']
    format_str = '%d/%m/%Y'
    date_start = datetime.strptime(request.form['started_at'], format_str)
    date_end = datetime.strptime(request.form['ended_at'], format_str)
    loan.newLoan(id_inventory, id_student, date_start, date_end)

    return Response(status=200)