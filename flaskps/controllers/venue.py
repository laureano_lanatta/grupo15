import requests
from flaskps.classes.forms.venue_create import VenueForm
from flask import request, Response, jsonify


def headers():
    return {'Authorization': 'Bearer TIONJAE3O3WSGF6E5SPZ'}


def index():
    response = requests.get('https://www.eventbriteapi.com/v3/organizations/380765618509/venues/', headers=headers())
    return jsonify(response.json()['venues'])


def create():
    form = VenueForm()
    if form.validate_on_submit():
        venue = {
            'venue': {
                'name': request.form['name'],
                'capacity': request.form['capacity'],
                'google_place_id': request.form['place_id'],
                'address': {
                    'address_1': request.form['address'],
			        'latitude': request.form['mapLat'],
			        'longitude': request.form['mapLng'],
                }
            }
        }
        response = requests.post('https://www.eventbriteapi.com/v3/organizations/380765618509/venues/', json=venue, headers=headers())
        return Response(status=response.status_code)
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response
