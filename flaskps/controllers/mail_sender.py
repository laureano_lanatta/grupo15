from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from flaskps import app, mail
from flaskps.models import user
from flask import url_for, redirect, Request, Response, jsonify, json, request, render_template
from flaskps import hashing
from flaskps.controllers.feedback import page_not_found
from flaskps.config import Config


def send_to(email):
    last_id = str(user.get_id_by_email(email))     
    dire = hashing.hash_value(email, salt='grupo15')
    msg = Message("Confirm Email", recipients=[email])
    link = Config.LINK + '/activate?id=' + last_id + '&hash=' + dire
    msg.body = "Your link is {}".format(link)    
    msg.html = "<a href={} >Click aqui para activar</a>".format(link)
    mail.send(msg)        


def invite(emails, event):
    msg = Message('Invitacion a evento', recipients=emails)
    name = event['name']['text']
    date = event['start']['local'].split('T')[0]
    time = event['start']['local'].split('T')[1]
    link = event['url']
    address = event['venue']['address']['address_1']
    msg.html = f'''
    <div style="background-color: #666; padding: 10px;">
    <div style="color: #666; font-size: 1px;">Invitación a evento</div>
        <div style="max-width: 500px; margin: 0 auto; font-family: sans-serif; overflow: hidden;">
            <h1 style="text-align: center; color: #fff;">{name}</h1>
            <p style="margin: 20px; text-align: center; font-size: 18px; line-height: 1.5; color: #fff; font-weight: bold;">La Orquesta Escuela de Berisso te invita a participar del evento "{name}" el dia {date} a las {time} horas en {address}.</p>
            <p style="margin: 20px; text-align: center; font-size: 18px; line-height: 1.5; color: #fff; font-weight: bold;">Podés confirmar tu asistencia en el siguiente link.</p>
            <div style="margin: 20px; text-align: center;"><a href="{link}" style="text-decoration: none; display: inline-block; background: #69262c; color: #fff; font-weight: bold; padding: 15px 20px; border-radius: 5px;">Confirmar</a></div>
        </div>
    </div>
    '''
    mail.send(msg)


def cancel_event(emails, event):
    msg = Message('Cancelacion de evento', recipients=emails)
    name = event['name']['text']
    date = event['start']['local'].split('T')[0]
    time = event['start']['local'].split('T')[1]
    link = event['url']
    address = event['venue']['address']['address_1']
    msg.html = f'''
    <div style="background-color: #666; padding: 10px;">
    <div style="color: #666; font-size: 1px;">Cancelacion de evento</div>
        <div style="max-width: 500px; margin: 0 auto; font-family: sans-serif; overflow: hidden;">
            <h1 style="text-align: center; color: #fff;">{name}</h1>
            <p style="margin: 20px; text-align: center; font-size: 18px; line-height: 1.5; color: #fff; font-weight: bold;">La Orquesta Escuela de Berisso lamenta informar de la cancelacion del evento "{name}" el dia {date} a las {time} horas en {address}.</p>
            <div style="margin: 20px; text-align: center;"><a href="{link}" style="text-decoration: none; display: inline-block; background: #69262c; color: #fff; font-weight: bold; padding: 15px 20px; border-radius: 5px;">Informacion</a></div>
        </div>
    </div>
    '''
    mail.send(msg)


def confirm_email(token_id, token_hash):
    email = str(user.get_email_by_id(token_id))
    if  hashing.check_value(token_hash,email, salt='grupo15'):
        cant = str(user.get_confirm_value_by_id(token_id))
        if cant == 'si':
            return page_not_found(400)
        else:
            return redirect('/activate/'+token_id+'/'+token_hash)
            #return render_template('user/activate_password.html', email = email )

    else:
        return page_not_found(400)
        
       
def check_link():
    id = request.form['id']
    token = request.form['hash']   
    email = str(user.get_email_by_id(id))    
    if  hashing.check_value(token, email, salt='grupo15'):
        cant = str(user.get_confirm_value_by_id(id))
        if cant == 'si':
            response = {
            'valid': 'False'      
            }
        else:
            response = {
            'valid': 'True'      
            }
    else:
        response = {
            'valid': 'False'      
            }
    return jsonify(response)        