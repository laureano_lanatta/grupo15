import os
from flask import request, Response, jsonify
from flaskps.models import instrument
from flaskps.models import instrument_type
from flask import current_app as app
from werkzeug.utils import secure_filename
from flaskps.classes.forms.instrument import InstrumentForm


def index():
    response = [{
        'id': instrument.InventoryItem.id,
        'name': instrument.InventoryItem.name,
        'type': {
                'id': instrument.ItemType.id,
                'name': instrument.ItemType.name
                },
        'inventory_number': instrument.InventoryItem.inventory_number,
        'photo': instrument.InventoryItem.photo,
    } for instrument in instrument.get_all()]
    return jsonify(response)


def get_instrument_profile(id):
    if (instrument.check_exist_by_id(id)):
        data = instrument.get_by_id(id)
        response = {
            'id': data.InventoryItem.id,
            'name': data.InventoryItem.name,
            'type': {
                'id': data.ItemType.id,
                'name': data.ItemType.name
            },
            'inventory_number': data.InventoryItem.inventory_number,
            'photo': data.InventoryItem.photo,
        }
        return jsonify(response)
    else:
        return Response(status=400)


def exist(id):
    if (instrument.check_exist_by_id(id)):
        response = {'value': 'True'}
    else:
        response = {'value': 'False'}
    return jsonify(response)


def update():
    form = InstrumentForm()
    if form.validate_on_submit():
        if instrument.check_exist_by_id(request.form['id']):
            if instrument_type.check_exist_by_id(request.form['ins_type']):
                if not instrument.check_exist_by_inventory_number(request.form['number']) or request.form['number'] == request.form['old_number']:
                    filename=""
                    if request.files:
                        if not allowed_image_filesize(request.form['photo_size']):
                            response = jsonify(["photo"])
                            response.status_code = 400
                            return response
                        else:
                            image = request.files["photo"]
                            if image.filename == "":
                                response = jsonify(["photo"])
                                response.status_code = 400
                                return response
                            if not allowed_image_extension(image.filename):
                                response = jsonify(["photo"])
                                response.status_code = 400
                                return response
                            else:
                                ext = image.filename.rsplit(".", 1)[1]
                                filename = secure_filename(
                                    "image_instrument_"+request.form['id']+"."+ext)
                                image.save(os.path.join(
                                    app.config['IMAGE_INSTRUMENT'], filename))
                    instrument.update(request.form['id'], request.form['name'],
                                      request.form['number'], request.form['ins_type'], filename)
                    return Response(status=200)
                else:
                    response = jsonify(["number"])
                    response.status_code = 400
                    return response
            else:
                response = jsonify(["ins_type"])
                response.status_code = 400
                return response
        else:
            response = jsonify(["id"])
            response.status_code = 400
            return response
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def create():
    form = InstrumentForm()
    if form.validate_on_submit():
        if instrument_type.check_exist_by_id(request.form['ins_type']):
            if not instrument.check_exist_by_inventory_number(request.form['number']):
                filename = ""
                if request.files:
                    if not allowed_image_filesize(request.form['photo_size']):
                        response = jsonify(["photo"])
                        response.status_code = 400
                        return response
                    else:
                        image = request.files["photo"]
                        if image.filename == "":
                            response = jsonify(["photo"])
                            response.status_code = 400
                            return response
                        if not allowed_image_extension(image.filename):
                            response = jsonify(["photo"])
                            response.status_code = 400
                            return response
                        else:
                            filename ="needId"
                lastId=instrument.create(request.form['name'],request.form['number'], request.form['ins_type'])
                if filename =="needId":
                    ext = image.filename.rsplit(".", 1)[1]
                    filename = secure_filename("image_instrument_"+str(lastId)+"."+ext)
                    if not os.path.isdir(app.config['IMAGE_INSTRUMENT']):
                        os.makedirs(app.config['IMAGE_INSTRUMENT'])
                    image.save(os.path.join(app.config['IMAGE_INSTRUMENT'], filename))
                    instrument.update(lastId,request.form['name'],request.form['number'],request.form['ins_type'],filename)
                return Response(status=200)
            else:
                response = jsonify(["number"])
                response.status_code = 400
                return response
        else:
            response = jsonify(["ins_type"])
            response.status_code = 400
            return response
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def delete():
    if instrument.check_exist_by_id(request.form['id']):
        instrument.delete(request.form['id'])
        return Response(status=200)
    else:
        response = jsonify("El instrumento que desea eliminar no existe")
        response.status_code = 400
        return response


def allowed_image_extension(filename):
    if not "." in filename:
        return False
    ext = filename.rsplit(".", 1)[1]
    if ext.upper() in app.config['ALLOW_IMAGE_EXTENSIONS']:
        return True
    else:
        return False


def allowed_image_filesize(filesize):
    if int(filesize) <= app.config['MAX_IMAGE_FILESIZE']:
        return True
    else:
        return False
