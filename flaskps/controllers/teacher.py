from flask import request, jsonify, Response
from flaskps.models import teacher
from flaskps.classes.forms.Teacher import updateTeacherForm
from datetime import datetime


def index():
    response = [{
        'id': teacher.id,
        'username': teacher.username,
        'email': teacher.email,
        'first_name': teacher.first_name,
        'last_name': teacher.last_name
    } for teacher in teacher.get_all()]
    return jsonify(response)


def enrolled(id_enrollment):
    response = [{
        'id': teacher.id,
        'username': teacher.username,
        'email': teacher.email,
        'first_name': teacher.first_name,
        'last_name': teacher.last_name
    } for teacher in teacher.get_enrolled(id_enrollment)]
    return jsonify(response) 


def notEnrolled(id_enrollment):
    enrolled_user_ids = [teacher.id for teacher in teacher.get_enrolled(id_enrollment)]
    response = [{
        'id': teacher.id,
        'username': teacher.username,
        'email': teacher.email,
        'first_name': teacher.first_name,
        'last_name': teacher.last_name
    } for teacher in teacher.get_all() if teacher.id not in enrolled_user_ids]
    return jsonify(response) 

    
def get_teacher_profile(id):
    if (teacher.check_exist_by_id(id) == 1):
        data = teacher.get_by_id(id)
        if data.birthday is None:
            date = data.birthday  
        else:
            date = data.birthday.strftime("%Y-%m-%d")
        response = {
        'teacher_data_first_name': data.first_name,
        'teacher_data_last_name': data.last_name,
        'teacher_data_dni': data.document_number,
        'teacher_data_dnitype': data.id_document_type,
        'teacher_data_birthday': date,
        'teacher_data_address': data.address,
        'teacher_data_phone': data.phone_number,
        'teacher_data_id_city': data.id_city,        
        }
        return jsonify(response)
    else:     
        return page_not_found(400)


def teacher_update(id):
    form = updateTeacherForm()
    if form.validate_on_submit():
        #vars
        fn = request.form['first_name_input']
        ln = request.form['last_name_input']
        address = request.form['address_input']
        dni = request.form['dni_input']
        dnitype = request.form['dnitype_input']
        city = request.form['city_input']     
        phone = request.form['phone_input']   
        birthday = request.form['birthday_input']

        #update
        teacher.update_teacher_info(id, fn, ln, birthday, address, dni, dnitype, phone, city)                            
        return Response(status=200)        
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response         


def teacher_create(form):   
    print('por aca paso')
    #vars
    email = form['email']
    fn = form['first_name']
    ln = form['last_name']
    address = form['address']
    dni = form['dni']
    dnitype = form['dnitype']
    city = form['city']     
    phone = form['phone']   
    birthday = form['birth']
    #update
    teacher.update_teacher_info_by_email(email, fn, ln, birthday, address, dni, dnitype, phone, city)                            
    return Response(status=200)        
         
