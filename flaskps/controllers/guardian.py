from flaskps.models import guardian, student
from flask import request, Response, jsonify, session
from flaskps.classes.forms.Guardian import addGuardianForm, updateGuardianForm


def index():
    response = [{
        'id': guardian.id,        
        'first_name': guardian.first_name,
        'last_name': guardian.last_name,
        'document_number': guardian.document_number,
        'dnitype': guardian.id_document_type,
        'birthday': guardian.birthday.strftime("%Y-%m-%d"),
        'address': guardian.address,
        'phone': guardian.phone_number,
        'id_city': guardian.id_city,
        'id_gender': guardian.id_gender
    } for guardian in guardian.get_all()]
    return jsonify(response)


def up():
    form = addGuardianForm()
    if form.validate_on_submit():
        fn = request.form['first_name']
        ln = request.form['last_name']
        phone = request.form['phone']
        address = request.form['address']
        id_c = request.form['city']
        birthday = request.form['birth']
        dni = request.form['dni']
        dnitype = request.form['dnitype']
        sex = request.form['sex']
        id_student = request.form['student']
        if guardian.check_exist_by_dni(dni) == 1:
            return Response(status=501)
        else:    
            guardian.create(fn, ln, phone, address, birthday, dni, dnitype, id_c, sex)           
            id_guardian = guardian.get_id_by_dni(dni)
            student.add_guardian(id_student, id_guardian)            
            return Response(status=200)
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def get_guardian_by_student(id):
    response = [{
    'id': guardian.id,
    'first_name': guardian.first_name,
    'last_name': guardian.last_name,
    'document_number': guardian.document_number,
    'dnitype': guardian.id_document_type,
    'birthday': guardian.birthday.strftime("%Y-%m-%d"),
    'address': guardian.address,
    'phone': guardian.phone_number,
    'id_city': guardian.id_city,
    'id_gender': guardian.id_gender,
    } for guardian in guardian.get_guardian_by_id_student(id)]
    return jsonify(response)


def get_profile(id):
    if (guardian.check_exist_by_id(id) == 1):
        data = guardian.get_by_id(id)        
        response = {
        'guardian_data_first_name': data.first_name,
        'guardian_data_last_name': data.last_name,
        'guardian_data_dni': data.document_number,
        'guardian_data_dnitype': data.id_document_type,
        'guardian_data_birthday': data.birthday.strftime("%Y-%m-%d"),
        'guardian_data_address': data.address,
        'guardian_data_phone': data.phone_number,
        'guardian_data_id_city': data.id_city,
        'guardian_data_id_gender': data.id_gender,
        }
        return jsonify(response)
    else:     
        return Response(status=400)   


def guardian_update(id):
    form = updateGuardianForm()
    if form.validate_on_submit():
        #vars
        fn = request.form['first_name_input']
        ln = request.form['last_name_input']
        address = request.form['address_input']
        dni = request.form['dni_input']
        dnitype = request.form['dnitype_input']
        city = request.form['city_input']     
        phone = request.form['phone_input']   
        birthday = request.form['birthday_input']       
        sex = request.form['sex_input']
        if guardian.get_dni_by_id(id) != dni:
            if guardian.check_exist_by_dni(dni) == 1:
                return Response(status=501)
        guardian.update_guardian_info(id, fn, ln, birthday, address, dni, dnitype, phone, city, sex)                            
        return Response(status=200)        
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response              


def exist(id):  
    if (guardian.check_exist_by_id(id) == 1):
        response = {'value':'True' }
    else:
        response = {'value':'False'}         
    return jsonify(response)