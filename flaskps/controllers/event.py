import requests
from flaskps.classes.forms.event_create import EventForm
from flaskps.classes.forms.inviteEmailList import InviteEmailListForm
from flask import request, Response, jsonify
from flaskps.controllers import mail_sender


def headers():
    return {'Authorization': 'Bearer TIONJAE3O3WSGF6E5SPZ'}


def index():
    response = requests.get('https://www.eventbriteapi.com/v3/organizations/380765618509/events/', headers=headers())
    events = [event for event in response.json()['events'] if event['capacity'] < 1000001]
    return jsonify(events)


def show(id):
    response = requests.get(f'https://www.eventbriteapi.com/v3/events/{id}/?expand=venue', headers=headers())
    if response.status_code == 200:
        return jsonify(response.json())
    else:
        return Response(status=response.status_code)


def create():
    form = EventForm()
    if form.validate_on_submit():
        event = {
            'event': {
                'name': {
                    'html': request.form['name']
                },
                'description': {
                    'html': f"<p>{request.form['description']}</p>"
                },
                'currency': 'ARS',
                'locale': 'es_AR',
                'format_id': 6,
                'category_id': 103,
                'venue_id': request.form['venue_id'],
                'capacity': request.form['capacity'],
                'start': {
                    'timezone': 'America/Argentina/Buenos_Aires',
                    'utc': request.form['date']
                },
                'end': {
                    'timezone': 'America/Argentina/Buenos_Aires',
                    'utc': request.form['date']
                }
            }
        }

        ticket = {
            'ticket_class': {
                'name': 'Entrada General',
                'free': True,
                'capacity': request.form['capacity']
            }
        }

        response = requests.post('https://www.eventbriteapi.com/v3/organizations/380765618509/events/', json=event, headers=headers())
        event_id = response.json()["id"]
        response = requests.post(f'https://www.eventbriteapi.com/v3/events/{event_id}/ticket_classes/', json=ticket, headers=headers())
        response = requests.post(f'https://www.eventbriteapi.com/v3/events/{event_id}/publish/', headers=headers())
        return Response(status=response.status_code)
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def delete(id):
    # Eventbrite API does not support 'delete order' yet
    # so... yeah... sry
    event = {
        'event': {
            'capacity': 1000001
        }
    }
    ticket_class = {
        'ticket_class': {
            'hidden': True
        }
    }
    requests.post(f'https://www.eventbriteapi.com/v3/events/{id}/', json=event, headers=headers())
    ticket_classes = requests.get(f'https://www.eventbriteapi.com/v3/events/{id}/ticket_classes/', headers=headers())
    ticket_class_id = ticket_classes.json()['ticket_classes'][0]['id']
    requests.post(f'https://www.eventbriteapi.com/v3/events/{id}/ticket_classes/{ticket_class_id}/', json=ticket_class, headers=headers())
    event = requests.get(f'https://www.eventbriteapi.com/v3/events/{id}/?expand=venue', headers=headers())
    attendees = requests.get(f'https://www.eventbriteapi.com/v3/events/{id}/attendees/', headers=headers())
    emails = list(set([email['profile']['email'] for email in attendees.json()['attendees']]))
    if emails:
        mail_sender.cancel_event(emails, event.json())
    return Response(status=200)


def invite(id):
    form = InviteEmailListForm()
    if form.validate_on_submit():
        event = requests.get(f'https://www.eventbriteapi.com/v3/events/{id}/?expand=venue', headers=headers())
        emails = [request.form[email] for email in request.form]
        mail_sender.invite(emails, event.json())
        return Response(status=200)
    else:
        print(form.errors)
        return Response(status=400)
