from flask import request, Response, jsonify, session
from flaskps.models import attendance,attendance_user
from datetime import datetime
import json

def index(id):
    response = [{
        'id': student.id,
        'first_name': student.first_name,
        'last_name': student.last_name,
        'attendance': False,
    } for student in attendance.get_student_not_attendance(id)]
    return jsonify(response)


def create():
    attendances = json.loads(request.form['attendances'])
    user= request.form['user']
    enrollment= request.form["enrollment"]
    if(len(attendances) > 0):
        today = datetime.now().date()
        for att in attendances:
            attendance.create(enrollment,att["id"],today)
        if not(attendance_user.check_exist_by_user_date(enrollment,today)):
            attendance_user.create(enrollment,user,today)
    return Response(status=200)

        


