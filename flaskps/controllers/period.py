from flask import request, Response, jsonify
from flaskps.models import period
from flaskps.classes.forms.period import PeriodForm
from datetime import datetime


def index():
    response = [{
        'id': period.id,
        'start': period.start.strftime("%d/%m/%Y"),
        'end': period.end.strftime("%d/%m/%Y"),
        'semester': period.semester,
    } for period in period.get_all()]
    return jsonify(response)


def get_profile(id):
    if (period.check_exist_by_id(id)):
        data = period.get_by_id(id)
        response = {
            'id': data.id,
            'start': data.start.strftime("%d/%m/%Y"),
            'end': data.end.strftime("%d/%m/%Y"),
            'semester': data.semester,
        }
        return jsonify(response)
    else:
        return Response(status=400)


# def get_enrollments():
#     response = [{
#         'id_period': enrollment[0],
#         'id_enrollment': enrollment[1],
#         'id_workshop': enrollment[2],
#         'workshop': enrollment[3],
#     } for enrollment in period.get_enrollments()]
#     return jsonify(response)


# def get_workshop():
#     response = [{
#         'id': workshop.id,
#         'name': workshop.name,
#     } for workshop in period.get_workshops()]
#     return jsonify(response)


def exist(id):
    if (period.check_exist_by_id(id)):
        response = {'value': 'True'}
    else:
        response = {'value': 'False'}
    return jsonify(response)


def create():
    form = PeriodForm()
    if form.validate_on_submit():
        format_str = '%d/%m/%Y'
        date_start = datetime.strptime(request.form['start'], format_str)
        date_end = datetime.strptime(request.form['end'], format_str)
        if(date_start > date_end):
            response = jsonify(["start", "end"])
            response.status_code = 400
            return response
        else:
            period.create(date_start, date_end, request.form['semester'])
            return Response(status=200)
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response


def update():
    form = PeriodForm()
    if form.validate_on_submit():
        if period.check_exist_by_id(request.form['id']):
            format_str = '%d/%m/%Y'
            date_start = datetime.strptime(request.form['start'], format_str)
            date_end = datetime.strptime(request.form['end'], format_str)
            if(date_start > date_end):
                response = jsonify(["start", "end"])
                response.status_code = 400
                return response
            else:
                period.update(request.form['id'], date_start,date_end, request.form['semester'])
                return Response(status=200)
        else:
            return Response(status=500)
    else:
        response = jsonify(list(form.errors.keys()))
        response.status_code = 400
        return response

def delete():
    if period.check_exist_by_id(request.form['id']):
        if not period.check_exist_enrollment(request.form['id']):
            period.delete(request.form['id'])
            return Response(status=200)
        else:
            return Response(status=400)
    else:
        return Response(status=200)


# def assign_workshop():
#     check1 = True
#     check2 = True
#     adds = json.loads(request.form['adds'])
#     removes = json.loads(request.form['removes'])
#     period_selected = request.form['period']
#     workshops = period.get_workshops_ids()
#     if period.check_exist_by_id(period_selected):
#         # Check that permissions are exist
#         for add in adds:
#             if add not in workshops:
#                 check1 = False
#                 break
#         for remove in removes:
#             if remove not in workshops:
#                 check2 = False
#                 break
#         # If exists, add and remove permissions
#         if check1 & check2:
#             for add in adds:
#                 period.create_enrollment(period_selected, add)
#             for remove in removes:
#                 period.delete_enrollment(period_selected, remove)
#             return Response(status=200)
#         else:
#             response = jsonify(
#                 "Algunos de los talleres asignados no existen. Quizas estos fueron actualizados y necesites recargar la pagina")
#             response.status_code = 400
#             return response
#     else:
#         response = jsonify("El ciclo lectivo seleccionado no existe")
#         response.status_code = 400
#         return response
