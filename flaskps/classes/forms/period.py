from flask_wtf import FlaskForm
from wtforms import DateField, RadioField
from wtforms.validators import DataRequired, Length, Regexp

class PeriodForm(FlaskForm):
    start=DateField(format='%d/%m/%Y')
    end=DateField(format='%d/%m/%Y')
    semester=RadioField(choices = [('1','1'), ('2','2')])