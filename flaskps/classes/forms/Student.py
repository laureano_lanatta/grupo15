from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import IntegerField
from wtforms import DateTimeField
from wtforms.validators import DataRequired, Length


class addStudentform(FlaskForm):
    first_name = StringField(validators=[DataRequired()])
    last_name = StringField(validators=[DataRequired()])
    phone = StringField(validators=[DataRequired()])
    address = StringField(validators=[DataRequired()])          
    dni = IntegerField(validators=[DataRequired()])
    dnitype = IntegerField(validators=[DataRequired()])
    city = IntegerField(validators=[DataRequired()])
    birthplace = IntegerField(validators=[DataRequired()])
    school = IntegerField(validators=[DataRequired()])
    neighborhood = IntegerField(validators=[DataRequired()])
    sex = IntegerField(validators=[DataRequired()])
    level = IntegerField(validators=[DataRequired()])

class updateStudentform(FlaskForm):
    first_name_input = StringField(validators=[DataRequired()])
    last_name_input  = StringField(validators=[DataRequired()])
    phone_input  = StringField(validators=[DataRequired()])
    address_input  = StringField(validators=[DataRequired()])          
    dni_input  = IntegerField(validators=[DataRequired()])
    dnitype_input  = IntegerField(validators=[DataRequired()])
    city_input  = IntegerField(validators=[DataRequired()])
    birthplace_input  = IntegerField(validators=[DataRequired()])
    school_input  = IntegerField(validators=[DataRequired()])
    neighborhood_input  = IntegerField(validators=[DataRequired()])
    sex_input  = IntegerField(validators=[DataRequired()])
    level_input  = IntegerField(validators=[DataRequired()])    


   
