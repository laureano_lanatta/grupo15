from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


class InstrumentTypeForm(FlaskForm):
    name = StringField(validators=[DataRequired()])