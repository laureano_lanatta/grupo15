from flask_wtf import FlaskForm
from wtforms import StringField, DateTimeField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class EventForm(FlaskForm):
    name = StringField(validators=[DataRequired()])

    description = StringField(
        validators=[DataRequired()])

    date = DateTimeField(
        validators=[DataRequired()], format='%Y-%m-%dT%H:%M:%SZ')

    venue_id = StringField(validators=[DataRequired()])

    capacity = IntegerField(validators=[DataRequired(), NumberRange(1)])
