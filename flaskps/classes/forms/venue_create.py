from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class VenueForm(FlaskForm):
    capacity = IntegerField(validators=[DataRequired(), NumberRange(1)])

    address = StringField(validators=[DataRequired()])

    name = StringField(validators=[DataRequired()])

    place_id = StringField(validators=[DataRequired()])

    mapLat = StringField(validators=[DataRequired()])

    mapLng = StringField(validators=[DataRequired()])