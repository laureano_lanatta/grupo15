from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import IntegerField
from wtforms import DateTimeField
from wtforms.validators import DataRequired, Length


class updateTeacherForm(FlaskForm):
    first_name_input = StringField(validators=[DataRequired()])
    last_name_input = StringField(validators=[DataRequired()])
    address_input = StringField(validators=[DataRequired()])
    dni_input = IntegerField(validators=[DataRequired()])
    dnitype_input = IntegerField(validators=[DataRequired()])
    city_input = IntegerField(validators=[DataRequired()])
    phone_input = IntegerField(validators=[DataRequired()])
    #birthday_input = DateTimeField(validators=[DataRequired()])

