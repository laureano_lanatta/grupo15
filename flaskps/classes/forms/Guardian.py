from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import IntegerField
from wtforms import DateTimeField
from wtforms.validators import DataRequired, Length


class addGuardianForm(FlaskForm):
    first_name = StringField(validators=[DataRequired()])
    last_name = StringField(validators=[DataRequired()])
    phone = StringField(validators=[DataRequired()])
    address = StringField(validators=[DataRequired()])          
    dni = IntegerField(validators=[DataRequired()])
    dnitype = IntegerField(validators=[DataRequired()])
    city = IntegerField(validators=[DataRequired()])
    sex = IntegerField(validators=[DataRequired()])
    

class updateGuardianForm(FlaskForm):
    first_name_input = StringField(validators=[DataRequired()])
    last_name_input  = StringField(validators=[DataRequired()])
    phone_input  = StringField(validators=[DataRequired()])
    address_input  = StringField(validators=[DataRequired()])          
    dni_input  = IntegerField(validators=[DataRequired()])
    dnitype_input  = IntegerField(validators=[DataRequired()])
    city_input  = IntegerField(validators=[DataRequired()])
    sex_input  = IntegerField(validators=[DataRequired()])


   
