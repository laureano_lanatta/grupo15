from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


class InstrumentForm(FlaskForm):
    name = StringField(validators=[DataRequired()])
    number = StringField(validators=[DataRequired()])
    ins_type = StringField(validators=[DataRequired()])