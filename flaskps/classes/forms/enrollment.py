from flask_wtf import FlaskForm
from wtforms import IntegerField,TimeField
from wtforms.validators import DataRequired


class EnrollmentForm(FlaskForm):
    period = IntegerField(validators=[DataRequired()])
    workshop = IntegerField(validators=[DataRequired()])
    center = IntegerField(validators=[DataRequired()])
    weekday = IntegerField(validators=[DataRequired()])
    hour=TimeField(format='%H:%M')