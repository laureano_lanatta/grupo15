from flaskps.models.classes.model import Attendance,Student
from flaskps.models import student
from flaskps.models.alchemy import db_session


def get_student_attendance(id):
    return db_session.query(Attendance.id_student).filter_by(id_enrollment=id).all()

def get_student_not_attendance(id):
    all_attendance=[
        student[0]
    for student in get_student_attendance(id)]
    my_students =[ 
         student[0]
    for student in student.get_id_enrolled(id)]
    not_attendance=db_session.query(Student).filter(Student.id.in_(my_students),Student.id.notin_(all_attendance)).all()
    return not_attendance

def amount(id_enrollment,id_student):
    return db_session.query(Attendance).filter_by(id_enrollment=id_enrollment,id_student=id_student).count()

def create(enrollment, student, today):
    new = Attendance(id_enrollment=enrollment, id_student=student,date=today)
    db_session.add(new)
    db_session.commit() 

