from flaskps.models.classes.model import Period
from flaskps.models.classes.model import Enrollment
from flaskps.models.classes.model import Workshop
from flaskps.models.alchemy import db_session

def get_all():
    return db_session.query(Period).all()

def create(start,end,semester):
    new = Period(start=start, end=end, semester=semester)
    db_session.add(new)
    db_session.commit()
    return new.id

def update(id, start , end , semester):        
    period = get_by_id(id)
    period.start = start          
    period.end = end        
    period.semester = semester   
    db_session.commit()

def delete(id):
    db_session.delete(get_by_id(id))
    db_session.commit()

def check_exist_enrollment(id):
    return db_session.query(Enrollment).filter_by(id_period=id).count()

def check_exist_by_id(id):
    return db_session.query(Period).filter_by(id=id).count()

def get_by_id(id):
    return db_session.query(Period).get(id) 

def get_enrollment_by_period_workshop(period,workshop):
    return db_session.query(Enrollment).filter_by(id_period=period,id_workshop=workshop)

def get_enrollments():
    result=db_session.query(Period.id,Enrollment.id,Workshop.id,Workshop.name).\
        join(Workshop, Enrollment.id_workshop == Workshop.id).\
        join(Period, Enrollment.id_period == Period.id).all()
    return result

def get_workshops():
    result=db_session.query(Workshop).all()
    return result

def get_workshops_ids():
    ids=[]
    result= db_session.query(Workshop.id).all()
    for elem in result:
        ids.append(elem[0])
    return ids


def create_enrollment(id_period,id_workshop):
    new = Enrollment(id_period=id_period, id_workshop=id_workshop)
    db_session.add(new)
    db_session.commit()

def delete_enrollment(id_period,id_workshop):
    Enrollment.query.filter_by(id_period= id_period,id_workshop=id_workshop).delete()
    db_session.commit()