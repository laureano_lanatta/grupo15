from flaskps.models.classes.model import Student, Level, Student_Enrollment, Enrollment,  Student_Guardian
from flaskps.models.alchemy import db_session
from flaskps.models import attendance,attendance_user

def get_all_small():
    return db_session.query(Student, Level).\
        join(Student.level).\
        all()


def get_enrolled(id):
    return db_session.query(Student, Level).\
        join(Student.level).\
        join(Student_Enrollment, Student.id == Student_Enrollment.id_student).\
        join(Enrollment, Student_Enrollment.id_enrollment == Enrollment.id).\
        filter_by(id = id).\
        all()

def get_enrolled_attendance(id):
    result=[]
    students=db_session.query(Student,Level).\
        join(Student.level).\
        join(Student_Enrollment, Student.id == Student_Enrollment.id_student).\
        join(Enrollment, Student_Enrollment.id_enrollment == Enrollment.id).\
        filter_by(id = id).\
        all()
    total_count=attendance_user.total_count(id)
    for student in students:
        count_attendance=attendance.amount(id,student.Student.id)
        result.append(
            {
                "id":student.Student.id,
                "first_name":student.Student.first_name,
                "last_name":student.Student.last_name,
                "level":student.Level.name,
                "count_attendance":count_attendance,
                "total_count":total_count
            }
        )
    return result


def get_id_enrolled(id):
    return db_session.query(Student.id).\
        join(Student_Enrollment, Student.id == Student_Enrollment.id_student).\
        join(Enrollment, Student_Enrollment.id_enrollment == Enrollment.id).\
        filter_by(id = id).\
        all()

def delete_student(id):
    db_session.delete(get_by_id(id))
    db_session.commit()

def get_all():
    return db_session.query(Student).all()

def get_by_id(id):
    return db_session.query(Student).get(id) 

def create(fn, ln, pn, address, id_l, id_c, birthday, dni, id_n, id_doc_t,id_s, birthplace, sex):
    new = Student(first_name=fn, last_name=ln, address=address, id_gender=sex, id_level=id_l, id_city=id_c, birthday=birthday, document_number=dni, id_neighborhood=id_n, id_document_type=id_doc_t, id_school=id_s, id_birthplace=birthplace, phone_number = pn )
    db_session.add(new)
    db_session.commit()    

def add_guardian(id_s, id_g):
    new = Student_Guardian(id_student=id_s, id_guardian=id_g)
    db_session.add(new)
    db_session.commit() 

def check_exist_by_dni(dni):
    return db_session.query(Student).filter_by(document_number=dni).count()    

def check_exist_by_id(id):
    return db_session.query(Student).filter_by(id=id).count()  

def get_dni_by_id(id):
    return db_session.query(Student).get(id).document_number

def get_id_by_dni(dni):
    return db_session.query(Student).filter_by(document_number=dni).first().id

def update_student_info(id, first_name, last_name, birthday, address, dni, dnitype, phone, city, school, level, neighborhood, birthplace, sex):        
    student = get_by_id(id)
    student.first_name = first_name        
    student.last_name = last_name 
    student.birthday = birthday 
    student.address = address 
    student.document_number = dni 
    student.id_document_type = dnitype 
    student.phone_number = phone 
    student.id_city = city  
    student.id_level = level  
    student.id_neighborhood = neighborhood  
    student.id_school = school 
    student.id_birthplace = birthplace
    student.id_gender = sex  


    db_session.commit()
