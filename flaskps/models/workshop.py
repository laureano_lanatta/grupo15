from flaskps.models.classes.model import Workshop, Enrollment
from flaskps.models.alchemy import db_session
from flask import session

def get_by_id(id):
    return db_session.query(Workshop).get(id) 

def validate(name):
    if db_session.query(Workshop).filter_by(name=name).count() == 1:
        return True
    else:
        return False 


def create_workshop(name):
    new = Workshop(name=name)
    db_session.add(new)
    db_session.commit()

def get_all():
    return db_session.query(Workshop).\
        all()


def delete_workshop(id):
    if db_session.query(Enrollment).filter_by(id_workshop=id).count() >= 1:
        return False
    else:
        db_session.delete(get_by_id(id))
        db_session.commit()
        return True

def updateWorkshop(id,name):
    print("llegue al modelo")
    workshop = get_by_id(id)
    workshop.name = name
    db_session.commit()

def check_exist_by_id(id):
    return db_session.query(Workshop).filter_by(id=id).count()
