from flaskps.models.classes.model import Loan, InventoryItem, Student
from flaskps.models.alchemy import db_session
from flask import session


def getAll():
    return db_session.query(Loan, Student, InventoryItem).\
    join(Student, Student.id == Loan.id_student).\
    join(InventoryItem, InventoryItem.id == Loan.id_inventory_item).all()

def newLoan(id_inventory, id_student, date_start, date_end):
    print("llege al modelo")
    newLoan = Loan(id_student=id_student,id_inventory_item=id_inventory, started_at=date_start, ended_at=date_end)
    print(newLoan)
    db_session.add(newLoan)
    db_session.commit()
        