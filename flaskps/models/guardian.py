from flaskps.models.classes.model import Guardian, Student_Guardian
from flaskps.models.alchemy import db_session

def get_all():
    return db_session.query(Guardian).all()

def get_by_id(id):
    return db_session.query(Guardian).get(id) 

def create(fn, ln, pn, address, birthday, dni, id_doc_t, birthplace, sex):
    new = Guardian(first_name=fn, last_name=ln, address=address, id_gender=sex, birthday=birthday, phone_number=pn, document_number=dni, id_document_type=id_doc_t, id_city=birthplace )
    db_session.add(new)
    db_session.commit()

def get_id_by_dni(dni):
    return db_session.query(Guardian).filter_by(document_number=dni).first().id

def get_guardian_by_id_student(id_student):
    return db_session.query(Guardian).\
        join(Student_Guardian, Student_Guardian.id_guardian == Guardian.id).\
        filter_by(id_student = id_student).all() 

def get_count_guardian_by_id_student(id_student):
    return db_session.query(Guardian).\
        join(Student_Guardian, Student_Guardian.id_guardian == Guardian.id).\
        filter_by(id_student = id_student).count()

def update_guardian_info(id, first_name, last_name, birthday, address, dni, dnitype, phone, city, sex):        
    guardian = get_by_id(id)
    guardian.first_name = first_name        
    guardian.last_name = last_name 
    guardian.birthday = birthday 
    guardian.address = address 
    guardian.document_number = dni 
    guardian.id_document_type = dnitype 
    guardian.phone_number = phone 
    guardian.id_city = city  
    guardian.id_gender = sex  
    db_session.commit()        

def check_exist_by_dni(dni):
    return db_session.query(Guardian).filter_by(document_number=dni).count()    

def check_exist_by_id(id):
    return db_session.query(Guardian).filter_by(id=id).count()  

def get_dni_by_id(id):
    return db_session.query(Guardian).get(id).document_number

def get_id_by_dni(dni):
    return db_session.query(Guardian).filter_by(document_number=dni).first().id     