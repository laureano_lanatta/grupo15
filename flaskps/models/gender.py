from flaskps.models.classes.model import Gender
from flaskps.models.alchemy import db_session

#Getter's
def get_all():
    return db_session.query(Gender).all()

def get_name_by_id(id):
    return db_session.query(Gender).filter_by(id = id).first().name    