from flaskps.models.classes.model import Enrollment, Workshop, Period, Center, Weekday, User_Enrollment, Student_Enrollment
from flaskps.models.alchemy import db_session


def get_all():
    return db_session.query(Enrollment, Workshop, Period, Center, Weekday).\
        join(Enrollment.workshop).\
        join(Enrollment.period).\
        join(Enrollment.center).\
        join(Enrollment.weekday).\
        all()


def get_by_id(id):
    return db_session.query(Enrollment, Workshop, Period, Center, Weekday).\
        filter_by(id = id).\
        join(Enrollment.workshop).\
        join(Enrollment.period).\
        join(Enrollment.center).\
        join(Enrollment.weekday).\
        first()

def get_by_id_only(id):
    return db_session.query(Enrollment).filter_by(id = id).first()


def get_id_by_period_workshop_center_weekday(id_period, id_workshop, id_center, id_weekday):
    return db_session.query(Enrollment).\
        filter_by(id_period = id_period, id_workshop = id_workshop, id_center = id_center, id_weekday = id_weekday).\
        first()


def get_by_student_id(id):
    return db_session.query(Enrollment, Workshop, Period, Center, Weekday).\
        join(Enrollment.workshop).\
        join(Enrollment.period).\
        join(Enrollment.center).\
        join(Enrollment.weekday).\
        join(Student_Enrollment, Enrollment.id == Student_Enrollment.id_enrollment).\
        filter_by(id_student = id).\
        all()


def get_by_teacher_id(id):
    return db_session.query(Enrollment, Workshop, Period, Center, Weekday).\
        join(Enrollment.workshop).\
        join(Enrollment.period).\
        join(Enrollment.center).\
        join(Enrollment.weekday).\
        join(User_Enrollment, Enrollment.id == User_Enrollment.id_enrollment).\
        filter_by(id_user = id).\
        all()


def enroll_teacher(id_enrollment, id_teacher):
    new = User_Enrollment(id_enrollment = id_enrollment, id_user = id_teacher)
    db_session.add(new)
    db_session.commit()


def enroll_student(id_enrollment, id_student):
    new = Student_Enrollment(id_enrollment = id_enrollment, id_student = id_student)
    db_session.add(new)
    db_session.commit()

def check_exist_with(id_period,id_workshop,id_center,id_weekday):
    return db_session.query(Enrollment).filter_by(id_period=id_period,id_workshop=id_workshop,id_center=id_center,id_weekday=id_weekday).count()

def check_exist_by_id(id):
    return db_session.query(Enrollment).filter_by(id=id).count()

def check_exist_by_enrollment_teacher(id_enrollment,id_user):
    return db_session.query(User_Enrollment).filter_by(id_enrollment=id_enrollment,id_user=id_user).count()


def create(id_period,id_workshop,id_center,id_weekday,hour):
    new = Enrollment(id_period=id_period,id_workshop=id_workshop,id_center=id_center,id_weekday=id_weekday,hour=hour)
    db_session.add(new)
    db_session.commit()
    return new.id

def delete(id):
    db_session.delete(get_by_id_only(id))
    db_session.commit()

def update(id, workshop, center, hour):        
    enrollment = get_by_id_only(id)
    enrollment.id_workshop = workshop        
    enrollment.id_center = center        
    enrollment.hour = hour        
    db_session.commit()
