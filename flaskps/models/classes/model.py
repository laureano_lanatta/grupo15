# coding: utf-8
from sqlalchemy import Column, DateTime, ForeignKey, String, Table, Time, text, Date, Integer, BigInteger
from sqlalchemy.orm import relationship
from flaskps.models.alchemy import Base

metadata = Base.metadata


class AppConfig(Base):
    __tablename__ = 'app_config'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)
    value = Column(String(255), nullable=False)

class Attendance(Base):
    __tablename__ = 'attendance'

    id = Column(Integer, primary_key=True)
    id_student = Column(ForeignKey('student.id'), nullable=False, index=True)
    id_enrollment = Column(ForeignKey('enrollment.id'), nullable=False, index=True)
    date = Column(Date, nullable=False)

    student = relationship('Student')
    enrollment = relationship('Enrollment')

class AttendanceUser(Base):
    __tablename__ = 'attendance_user'

    id = Column(Integer, primary_key=True)
    id_enrollment = Column(ForeignKey('enrollment.id'), nullable=False, index=True)
    id_user = Column(ForeignKey('user.id'), nullable=False, index=True)
    date = Column(Date, nullable=False)

    user = relationship('User')
    enrollment = relationship('Enrollment')


class Center(Base):
    __tablename__ = 'center'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)
    address = Column(String(45))
    city = Column(String(45))
    phone_number = Column(String(45))
    lat = Column(String(45))
    lng = Column(String(45))




class Enrollment(Base):
    __tablename__ = 'enrollment'

    id = Column(Integer, primary_key=True)
    id_workshop = Column(ForeignKey('workshop.id'), nullable=False, index=True)
    id_period = Column(ForeignKey('period.id'), nullable=False, index=True)
    id_center = Column(ForeignKey('center.id'), nullable=False, index=True)
    id_weekday = Column(ForeignKey('weekday.id'), nullable=False, index=True)
    hour = Column(Time, nullable=False)

    period = relationship('Period')
    workshop = relationship('Workshop')
    center = relationship('Center')
    weekday = relationship('Weekday')
    student = relationship('Student', secondary='student_enrollment')
    user = relationship('User', secondary='user_enrollment')


class Gender(Base):
    __tablename__ = 'gender'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)


class Guardian(Base):
    __tablename__ = 'guardian'

    id = Column(Integer, primary_key=True)
    first_name = Column(String(45), nullable=False)
    last_name = Column(String(45), nullable=False)
    phone_number = Column(BigInteger)
    address = Column(String(45), nullable=False)
    id_city = Column(Integer, nullable=False)
    birthday = Column(Date, nullable=False)
    document_number = Column(String(45), nullable=False)
    id_document_type = Column(Integer, nullable=False)
    id_gender = Column(ForeignKey('gender.id'), index=True)

    gender = relationship('Gender')
    student = relationship('Student', secondary='student_guardian')


class InventoryItem(Base):
    __tablename__ = 'inventory_item'

    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    id_item_type = Column(ForeignKey('item_type.id'), nullable=False, index=True)
    name = Column(String(45), nullable=False)
    photo = Column(String(45), nullable=False)
    inventory_number = Column(Integer, primary_key=True)
    item_type = relationship('ItemType')

class ItemLog(Base):
    __tablename__ = 'item_log'

    id = Column(Integer, primary_key=True)
    description = Column(String(255), nullable=False)
    id_inventory_item = Column(ForeignKey('inventory_item.id', ondelete='CASCADE'), nullable=False, index=True)

    inventory_item = relationship('InventoryItem')


class ItemType(Base):
    __tablename__ = 'item_type'

    id = Column(Integer, primary_key=True)
    name = Column(String(45))


class Level(Base):
    __tablename__ = 'level'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)


class Loan(Base):
    __tablename__ = 'loan'

    id = Column(Integer, primary_key=True)
    id_student = Column(ForeignKey('student.id'), nullable=False, index=True)
    id_inventory_item = Column(ForeignKey('inventory_item.id'), nullable=False, index=True)
    started_at = Column(DateTime, nullable=False)
    ended_at = Column(DateTime)

    inventory_item = relationship('InventoryItem')
    student = relationship('Student')


class Neighborhood(Base):
    __tablename__ = 'neighborhood'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)


class Period(Base):
    __tablename__ = 'period'

    id = Column(Integer, primary_key=True)
    start = Column(Date, nullable=False)
    end = Column(Date, nullable=False)
    semester = Column(String(45))


class Permission(Base):
    __tablename__ = 'permission'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)
    public_name = Column(String(45))

    role = relationship('Role', secondary='role_permission')


class Role(Base):
    __tablename__ = 'role'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)

    user = relationship('User', secondary='user_role')


class Role_Permission(Base):
    __tablename__ = 'role_permission'
    
    id_role = Column(ForeignKey('role.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    id_permission = Column(ForeignKey('permission.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)


class School(Base):
    __tablename__ = 'school'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)
    address = Column(String(45))
    telephone = Column(String(45))


class Student(Base):
    __tablename__ = 'student'

    id = Column(Integer, primary_key=True)
    first_name = Column(String(45), nullable=False)
    last_name = Column(String(45), nullable=False)
    phone_number = Column(BigInteger)
    address = Column(String(45), nullable=False)
    id_level = Column(ForeignKey('level.id'), nullable=False, index=True)
    id_city = Column(Integer, nullable=False)
    birthday = Column(Date, nullable=False)
    id_birthplace = Column(Integer)
    document_number = Column(String(45), nullable=False)
    id_neighborhood = Column(ForeignKey('neighborhood.id'), nullable=False, index=True)
    id_document_type = Column(Integer, nullable=False)
    id_school = Column(ForeignKey('school.id'), nullable=False, index=True)
    id_gender = Column(ForeignKey('gender.id'), index=True)

    gender = relationship('Gender')
    level = relationship('Level')
    neighborhood = relationship('Neighborhood')
    school = relationship('School')


class Student_Enrollment(Base):

    __tablename__ = 'student_enrollment'

    id_student = Column(ForeignKey('student.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    id_enrollment = Column(ForeignKey('enrollment.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)


class Student_Guardian(Base):
    __tablename__ = 'student_guardian'

    id_student = Column(ForeignKey('student.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    id_guardian = Column(ForeignKey('guardian.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String(45), nullable=False, unique=True)
    username = Column(String(45))
    password = Column(String(100))
    first_name = Column(String(45))
    last_name = Column(String(45))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime)
    bloqued = Column(String(45), nullable=False, server_default=text("'no'"))
    confirmed = Column(String(45), nullable=False, server_default=text("'no'"))
    id_city = Column(Integer)
    phone_number = Column(BigInteger)
    address = Column(String(45))
    birthday = Column(Date)
    document_number = Column(String(45))
    id_document_type = Column(Integer)
    photo = Column(String(45), nullable=False)



class User_Enrollment(Base):
    __tablename__ = 'user_enrollment'

    id_user = Column(ForeignKey('user.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    id_enrollment = Column(ForeignKey('enrollment.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)


class User_role(Base):
    __tablename__ = 'user_role'
    
    id_user = Column(ForeignKey('user.id', ondelete='CASCADE'), primary_key=True, nullable=False)
    id_role = Column(ForeignKey('role.id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)


class Weekday(Base):
    __tablename__ = 'weekday'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)


class Workshop(Base):
    __tablename__ = 'workshop'

    id = Column(Integer, primary_key=True)
    name = Column(String(45), nullable=False, unique=True)
