from flaskps.models.classes.model import InventoryItem,ItemType
from flaskps.models.alchemy import db_session


def get_all():
    return db_session.query(InventoryItem,ItemType).\
        join(ItemType,InventoryItem.id_item_type == ItemType.id).all()

def get_by_id(id):
    return db_session.query(InventoryItem,ItemType).\
        filter_by(id = id).\
        join(ItemType,InventoryItem.id_item_type == ItemType.id).first()

def get_by_id_only(id):
    return db_session.query(InventoryItem).filter_by(id = id).first()

def check_exist_by_id(id):
    return db_session.query(InventoryItem).filter_by(id=id).count()

def check_exist_by_inventory_number(number):
    return db_session.query(InventoryItem).filter_by(inventory_number=number).count()  

def create(name, number, ins_type):
    new = InventoryItem(name=name, inventory_number=number, id_item_type=ins_type)
    db_session.add(new)
    db_session.commit()
    return new.id

def update(id, name, number, ins_type, photo):        
    instrument = get_by_id(id)
    instrument.InventoryItem.name = name        
    instrument.InventoryItem.inventory_number = number 
    instrument.InventoryItem.id_item_type = ins_type 
    if(photo != ""):
        instrument.InventoryItem.photo = photo
    db_session.commit()

def delete(id):
    db_session.delete(get_by_id_only(id))
    db_session.commit()    