from flaskps.models.classes.model import ItemType,InventoryItem
from flaskps.models.alchemy import db_session


def get_all():
    return db_session.query(ItemType).all()

def get_by_id(id):
    return db_session.query(ItemType).filter_by(id=id).first() 

def check_exist_by_id(id):
    return db_session.query(ItemType).filter_by(id=id).count()  

def check_exist_by_name(name):
    return db_session.query(ItemType).filter_by(name=name).count()

def check_exist_instrument_with_type(id):
    return db_session.query(InventoryItem).filter_by(id_item_type=id).count()

def create(name):
    new = ItemType(name=name)
    db_session.add(new)
    db_session.commit()
    return new.id

def delete(id):
    db_session.delete(get_by_id(id))
    db_session.commit() 