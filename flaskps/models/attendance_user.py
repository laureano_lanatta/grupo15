from flaskps.models.classes.model import AttendanceUser
from flaskps.models.alchemy import db_session


def check_exist_by_user_date(enrollment, today):
    return db_session.query(AttendanceUser).filter_by(id_enrollment=enrollment, date=today).count()


def create(enrollment, user, today):
    new = AttendanceUser(id_enrollment=enrollment, id_user=user,date=today)
    db_session.add(new)
    db_session.commit() 

def total_count(id_enrollment):
    return db_session.query(AttendanceUser).filter_by(id_enrollment=id_enrollment).count()
