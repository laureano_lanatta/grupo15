from flaskps.models.classes.model import User, Role, User_role, User_Enrollment, Enrollment
from flaskps.models.alchemy import db_session

def get_all():
    return db_session.query(User).\
        join(User_role, User.id == User_role.id_user).\
        join(Role, User_role.id_role == Role.id).\
        filter_by(name = 'teacher').\
        all()


def get_by_id(id):   
    return db_session.query(User).\
        filter_by(id = id).\
        join(User_role, User.id == User_role.id_user).\
        join(Role, User_role.id_role == Role.id).\
        filter_by(name = 'teacher').\
        first()


def get_by_email(email):   
    return db_session.query(User).\
        filter_by(email = email).\
        join(User_role, User.id == User_role.id_user).\
        join(Role, User_role.id_role == Role.id).\
        filter_by(name = 'teacher').\
        first()


def check_exist_by_id(id):   
    return db_session.query(User).\
        filter_by(id = id).\
        join(User_role, User.id == User_role.id_user).\
        join(Role, User_role.id_role == Role.id).\
        filter_by(name = 'teacher').\
        count()


def get_enrolled(id):
    return db_session.query(User).\
        join(User_Enrollment, User.id == User_Enrollment.id_user).\
        join(Enrollment, User_Enrollment.id_enrollment == Enrollment.id).\
        filter_by(id = id).\
        all()


def update_teacher_info(id, first_name, last_name, birthday, address, dni, dnitype, phone, id_city):        
    teacher = get_by_id(id)
    teacher.first_name = first_name        
    teacher.last_name = last_name 
    teacher.birthday = birthday 
    teacher.address = address 
    teacher.document_number = dni 
    teacher.id_document_type = dnitype 
    teacher.phone_number = phone 
    teacher.id_city = id_city   
    db_session.commit()

def update_teacher_info_by_email(email, first_name, last_name, birthday, address, dni, dnitype, phone, id_city):        
    teacher = get_by_email(email)
    teacher.first_name = first_name        
    teacher.last_name = last_name 
    teacher.birthday = birthday 
    teacher.address = address 
    teacher.document_number = dni 
    teacher.id_document_type = dnitype 
    teacher.phone_number = phone 
    teacher.id_city = id_city   
    db_session.commit()
