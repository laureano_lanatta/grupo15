from flaskps.models.classes.model import Level, Student
from flaskps.models.alchemy import db_session

#Getter's
def get_all():
    return db_session.query(Level).all()

def get_name_by_id(id):
    return db_session.query(Level).filter_by(id = id).first().name    


def get_cant_level(id):
    return db_session.query(Level).filter_by(id = id).\
        join(Student, Student.id_level == Level.id).count()

