from flaskps.models.classes.model import Weekday
from flaskps.models.alchemy import db_session

def get_all():
    return db_session.query(Weekday).all()

def check_exist_by_id(id):
    return db_session.query(Weekday).filter_by(id=id).count()