import About from '../views/About.vue'
import ActivatePassword from '../views/ActivatePassword.vue'
import Administrator from '../views/Administrator.vue'
import axios from 'axios'
import Center from '../views/Center.vue'
import Configuration from '../views/Configuration.vue'
import Contact from '../views/Contact.vue'
import Course from '../views/Course.vue'
import CourseTeacher from '../views/CourseTeacher.vue'
import EditWorkshop from '../views/EditWorkshop.vue'
import EnrollmentDetail from '../views/EnrollmentDetail.vue'
import EnrollmentStudent from '../views/EnrollmentStudent.vue'
import EnrollmentTeacher from '../views/EnrollmentTeacher.vue'
import Feedback from '../views/Feedback.vue'
import Home from '../views/Home.vue'
import Instrument from '../views/Instrument.vue'
import InstrumentType from '../views/InstrumentType.vue'
import Login from '../views/Login.vue'
import NewCenter from '../views/NewCenter.vue'
import NewEnrollment from '../views/NewEnrollment.vue'
import NewGuardian from '../views/NewGuardian.vue'
import NewInstrument from '../views/NewInstrument.vue'
import NewPeriod from '../views/NewPeriod.vue'
import NewStudent from '../views/NewStudent.vue'
import NewUser from '../views/NewUser.vue'
import NewWorkshop from '../views/NewWorkshop.vue'
import NotFound from '../views/NotFound.vue'
import Period from '../views/Period.vue'
import Preceptor from '../views/Preceptor.vue'
import Profile_User from '../views/Profile_User.vue'
import Profile_Center from '../views/Profile_Center.vue'
import Profile_Course from '../views/Profile_Course.vue'
import Profile_Guardian from '../views/Profile_Guardian.vue'
import Profile_Instrument from '../views/Profile_Instrument.vue'
import Profile_Period from '../views/Profile_Period.vue'
import Profile_Student from '../views/Profile_Student.vue'
import Profile_Teacher from '../views/Profile_Teacher.vue'
import Roles from '../views/Roles.vue'
import store from '../store'
import Student from '../views/Student.vue'
import StudentDetail from '../views/StudentDetail.vue'
import TakeAttendance from '../views/TakeAttendance.vue'
import Teacher from '../views/Teacher.vue'
import TeacherDetail from '../views/TeacherDetail.vue'
import User from '../views/User.vue'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Workshop from '../views/Workshop.vue'
import Event from '../views/Event.vue'
import NewEvent from '../views/NewEvent.vue'
import EventDetail from '../views/EventDetail.vue'
import Main from '../views/Main.vue'
import NewVenue from '../views/NewVenue.vue'
import EventInvite from '../views/EventInvite.vue'
import Loan from '../views/Loan.vue'
import NewLoan from '../views/NewLoan.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/feedback',
    name: 'feedback',
    component: Feedback,
    props: true
  },
  {
    path: '/activate',
    name: 'activate',
    component: ActivatePassword 
  },
  {
    path: '/',
    component: Main,
    children: [
      {
        path: '/usuario',
        name: 'user_index',
        component: User,
        beforeEnter: requiresAuth
      },
      {
        path: '/usuario/nuevo',
        name: 'user_new',
        component: NewUser,
        beforeEnter: requiresAuth
      },
      {
        path: '/usuario/:id',
        name: 'user_profile',
        component: Profile_User,
        beforeEnter: requiresAuth
      },
      {
        path: '/docente',
        name: 'teacher_index',
        component: Teacher,
        beforeEnter: requiresAuth
      },
      {
        path: '/docente/:id',
        name: 'teacher_profile',
        component: Profile_Teacher,
        beforeEnter: requiresAuth
      },
      {
        path: '/docente/:id/detalle',
        name: 'teacher_detail',
        component: TeacherDetail,
        beforeEnter: requiresAuth
      },
      {
        path: '/administrador',
        name: 'administrator_index',
        component: Administrator,
        beforeEnter: requiresAuth
      },
      {
        path: '/preceptores',
        name: 'preceptor_index',
        component: Preceptor,
        beforeEnter: requiresAuth
      },
      {
        path: '/alumno',
        name: 'student_index',
        component: Student,
        beforeEnter: requiresAuth
      },
      {
        path: '/alumno/nuevo',
        name: 'student_new',
        component: NewStudent,
        beforeEnter: requiresAuth
      },
      {
        path: '/alumno/:id',
        name: 'student_profile',
        component: Profile_Student,
        beforeEnter: requiresAuth
      },
      {
        path: '/alumno/:id/nuevoResponsable',
        name: 'guardian_new',
        component: NewGuardian,
        beforeEnter: requiresAuth
      },
      {
        path: '/alumno/:id/detalle',
        name: 'student_detail',
        component: StudentDetail,
        beforeEnter: requiresAuth
      },
      {
        path: '/responsable/:id',
        name: 'guardian_profile',
        component: Profile_Guardian,
        beforeEnter: requiresAuth
      },
      {
        path: '/configuracion',
        name: 'configuration_index',
        component: Configuration,
        beforeEnter: requiresAuth
      },
      {
        path: '/roles',
        name: 'roles_index',
        component: Roles,
        beforeEnter: requiresAuth
      },
      {
        path: '/contacto',
        name: 'home_contact',
        component: Contact,
        beforeEnter: requiresAuth
      },
      {
        path: '/acerca',
        name: 'home_about',
        component: About,
        beforeEnter: requiresAuth
      },
      {
        path: '/inscripcion/alumno',
        name: 'enrollment_student_index',
        component: EnrollmentStudent,
        beforeEnter: requiresAuth
      },
      {
        path: '/inscripcion/docente',
        name: 'enrollment_teacher_index',
        component: EnrollmentTeacher,
        beforeEnter: requiresAuth
      },
      {
        path: '/instrumento',
        name: 'instrument_index',
        component: Instrument,
        beforeEnter: requiresAuth
      },
      {
        path: '/instrumento/nuevo',
        name: 'instrument_new',
        component: NewInstrument,
        beforeEnter: requiresAuth
      },
      {
        path: '/instrumento/:id',
        name: 'instrument_profile',
        component: Profile_Instrument,
        beforeEnter: requiresAuth
      },
      {
        path: '/tipo_instrumento/',
        name: 'instrument_type_index',
        component: InstrumentType,
        beforeEnter: requiresAuth
      },
      {
        path: '/taller',
        name: 'workshop_index',
        component: Workshop,
        beforeEnter: requiresAuth
      },
      {
        path: '/ciclo',
        name: 'period_index',
        component: Period,
        beforeEnter: requiresAuth
      },
      {
        path: '/ciclo/nuevo',
        name: 'period_new',
        component: NewPeriod,
        beforeEnter: requiresAuth
      },
      {
        path: '/ciclo/:id',
        name: 'period_profile',
        component: Profile_Period,
        beforeEnter: requiresAuth
      },
      {
        path: '/nuevoTaller',
        name: 'new_workshop',
        component: NewWorkshop,
        beforeEnter: requiresAuth
      },
      {
        path: '/editarTaller/:id',
        name: 'edit_workshop',
        component: EditWorkshop,
        beforeEnter: requiresAuth
      },
      {
        path: '/inscripcion/:id',
        name: 'enrollment_detail',
        component: EnrollmentDetail,
        beforeEnter: requiresAuth
      },
      {
        path: '/nucleo',
        name: 'center_index',
        component: Center,
        beforeEnter: requiresAuth
      },      
      {
        path: '/nucleo/nuevo',
        name: 'center_new',
        component: NewCenter,
        beforeEnter: requiresAuth
      },
      {
        path: '/nucleo/:id',
        name: 'center_profile',
        component: Profile_Center,
        beforeEnter: requiresAuth
      },
      {
        path: '/curso',
        name: 'course_index',
        component: Course,
        beforeEnter: requiresAuth
      },
      {
        path: '/curso/nuevo',
        name: 'course_new',
        component: NewEnrollment,
        beforeEnter: requiresAuth
      },
      {
        path: '/curso/:id',
        name: 'course_profile',
        component: Profile_Course,
        beforeEnter: requiresAuth
      },
      {
        path: '/asistencia/:id',
        name: 'attendance_index',
        component: TakeAttendance,
        beforeEnter: requiresAuth
      },
      {
        path: '/mis_cursos',
        name: 'course_teacher_index',
        component: CourseTeacher,
        beforeEnter: requiresAuth
      },
      {
        path: '/evento',
        name: 'event_index',
        component: Event,
        beforeEnter: requiresAuth
      },
      {
        path: '/evento/nuevo',
        name: 'event_new',
        component: NewEvent,
        beforeEnter: requiresAuth
      },
      {
        path: '/evento/:id',
        name: 'event_detail',
        component: EventDetail,
        beforeEnter: requiresAuth
      },
      {
        path: '/evento/:id/invitar',
        name: 'event_invite',
        component: EventInvite,
        beforeEnter: requiresAuth
      },
      {
        path: '/lugar/nuevo',
        name: 'venue_new',
        component: NewVenue,
        beforeEnter: requiresAuth
      },
      {
      path: '/prestamo',
      name: 'loan_index',
      component: Loan,
      },
      {
      path: '/nuevoPrestamo',
      name: 'new_loan',
      component: NewLoan,
      },
      {
        // This is the default view for Main
        // It has to be the last children
        path: '',
        name: 'home_index',
        component: Home,
        beforeEnter: requiresAuth
      }
    ]
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

function requiresAuth(to, from, next) {
    if (store.getters.getStatus != 1) {
      if (!store.getters.getUser) {
        store.dispatch('destroyUser')
        router.push('/login').catch(error => {})
      }
    }
    axios.get('/authorize/' + to.name)
      .then(response => next())
      .catch(error => {
        let options = {}
        switch (error.response.data.exception) {
          case 'UserHasNoSession':
            store.dispatch('destroyUser')
            router.push('/login').catch(error => {})
            break

          case 'RestrictedAcess':
            options = {
              title: 'Acceso restringido',
              description: 'No tiene permiso para acceder a este sitio.',
              noBacktrack: true
            }
            router.push({ name: 'feedback', params: options }).catch(error => {})
            break

          case 'UserIsBloqued':
            options = {
              title: 'Acceso restringido',
              description: 'Sus credenciales se encuentran deshabilitadas por el momento.',
              noBacktrack: true
            }
            router.push({ name: 'feedback', params: options }).catch(error => {})
            break

          case 'AppInMaintenanceMode':
            options = {
              title: 'Estamos trabajando',
              description: 'El sitio se encuentra en mantenimiento.',
              noBacktrack: true
            }
            router.push({ name: 'feedback', params: options }).catch(error => {})
            break

          default:
            break
        }       
      })
}

export default router
