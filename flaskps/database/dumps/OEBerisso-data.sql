--
-- Dumping data for table `app_config`
--

LOCK TABLES `app_config` WRITE;
/*!40000 ALTER TABLE `app_config` DISABLE KEYS */;
INSERT INTO `app_config` VALUES (1,'app_title','OEBerisso');
INSERT INTO `app_config` VALUES (2,'app_description','Esta es la Orquesta Escuela de Berisso');
INSERT INTO `app_config` VALUES (3,'app_contact_email','oeberisso@gmail.com');
INSERT INTO `app_config` VALUES (4,'items_per_page','5');
INSERT INTO `app_config` VALUES (5,'is_maintenance_mode','false');
/*!40000 ALTER TABLE `app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (1,'roxaneseel@gmail.com','5279828c9b7d6734fee2a3a504594f8cda7a751c5511739a921a5c08da8321ab','Roxane','Seel','2019-05-27 21:21:31','no','si',1,'100123456','Calle AAA, e/ B y C','1960-01-1','20123456',1);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`) VALUES (2,'claricecorbell@yahoo.com','bade03f729a90402607279212f89e92c3f1bef3f57cefec09298744532424a0b','Clarice','Corbell','2019-05-27 21:21:32','no','si');
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`) VALUES (3,'gisellagallon@outlook.com','f740ad1962358fd747d05a7f8af0cd8d199293302190ca7fc1dd56ca241cd70c','Gisella','Gallon','2019-05-27 21:21:33','no','si');
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (4,'rebekahdorning@gmail.com','eb6f7fe1c529fc5aa2922c1c8292d84a16ac228995b1a1970c638af80069ce1e','Rebekah','Dorning','2019-05-27 21:21:34','no','si',1,'111123456','Calle BBB, e/ D y E','1960-02-2','22123456',1);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`) VALUES (5,'joshuastine@gmail.com','5b1adba6786466a04ed17a5d5d1ea72504259d985a9e4e40cf6a844bdb02e21a','Joshua ','Stine','2019-05-27 21:21:35','no','si');
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (6,'fredthornhillg@gmail.com','b5204c8febb77e854628b4079cab50cf19b3b275ae9b90a9ec9272ecbe3091b4','Fred ','Thornhill','2019-05-27 21:21:36','no','no',2,'222123456','Calle CCC, e/ F y G','1960-02-3','22123456',2);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (7,'melissawynne@gmail.com','740d070cf7b8539632fd83bcee87387258310803e3bbe435a0ebec03d028044c','Melissa ','Wynne','2019-05-27 21:21:37','si','si',3,'333123456','Calle DDD, e/ H y I','1965-01-1','23123456',1);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (8,'seanstine@hotmail.com','99c2fbbffd2780ae2b7758dd9464519e1f73fea6b71628d222a943a91930135f','Sean ','Stine','2019-05-27 21:21:38','si','si',4,'444123456','Calle EEE, e/ J y K','1971-05-6','24123456',2);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`) VALUES (9,'royfrisch@gmail.com','479554e1fefc8abfea09f18321e7001aa79724a22ec4bdd69e26a3b89298c7ae','Roy ','Frisch','2019-05-27 21:21:39','si','si');
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`) VALUES (10,'melissavansickleg@gmail.com','3ff78840439da7f58c3aff40d62db9eaa3902e0a9c216fa29ad9086974a89df9','Melissa ','Vansickle','2019-05-27 21:21:40','si','si');
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (11,'florencestaggs@outlook.com','82098802979556ce9302fb98df952455621a661b259bc7726b641ccee07f8f42','Florence ','Staggs','2019-05-27 21:21:41','no','no',1,'555123456','Calle FFF, e/ L y M','1961-01-1','25123456',3);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (12,'benedictmeyer@gmail.com','8ab9f0b9745095189fb3ec749fd1740d68f5c16ff0e66f0af9843e1c21232ee5','Benedict ','Meyer','2019-05-27 21:21:42','no','no',1,'666123456','Calle GGG, e/ N y O','1972-02-7','26123456',4);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (13,'johnsmith@gmail.com','5b1adba6786466a04ed17a5d5d1ea72504259d985a9e4e40cf6a844bdb02e21a','John','Smith','2019-05-27 21:21:43','no','si',2,'777123456','Calle HHH, e/ P y Q','1960-06-6','27123456',1);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (14,'alisonserrato@gmail.com','ecf7ae9f12b5079b37ac049557813eab6f85840b5c4718a8f1bc1321d14bdbd4','Alison ','Serrato','2019-05-27 21:21:44','no','si',2,'888123456','Calle III, e/ R y S','1961-01-1','28123456',2);
INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `created_at`, `bloqued`, `confirmed`, `id_city`, `phone_number`, `address`, `birthday`, `document_number`, `id_document_type`) VALUES (15,'johnsmith@yahoo.com','5b1adba6786466a04ed17a5d5d1ea72504259d985a9e4e40cf6a844bdb02e21a','John','Smith','2019-05-27 21:21:45','no','si',4,'999123456','Calle JJJ, e/ T y U','1961-01-1','29123456',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'administrator');
INSERT INTO `role` VALUES (2,'preceptor');
INSERT INTO `role` VALUES (3,'teacher');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (1,'home_index','home');
INSERT INTO `permission` VALUES (2,'home_about','about');
INSERT INTO `permission` VALUES (3,'auth_login','login');
INSERT INTO `permission` VALUES (4,'auth_authenticate','autenticacion');
INSERT INTO `permission` VALUES (5,'auth_logout','logout');
INSERT INTO `permission` VALUES (6,'configuration_index','acceso configuracion');
INSERT INTO `permission` VALUES (7,'configuration_roles','roles');
INSERT INTO `permission` VALUES (8,'configuration_update_pagination','actualizar paginacion');
INSERT INTO `permission` VALUES (9,'configuration_update_public_info','actualizar info publica');
INSERT INTO `permission` VALUES (10,'configuration_toggle_maintenance_mode','modo mantenimiento');
INSERT INTO `permission` VALUES (11,'user_index','acceso a usuario');
INSERT INTO `permission` VALUES (12,'home_contact','contactos');
INSERT INTO `permission` VALUES (13,'configuration_create_role','crear rol');
INSERT INTO `permission` VALUES (14,'configuration_update_role','actualizar rol');
INSERT INTO `permission` VALUES (15,'configuration_delete_role','eliminar rol');
INSERT INTO `permission` VALUES (16,'configuration_show_roles','mostrar roles');
INSERT INTO `permission` VALUES (17,'user_add','agregar usuario');
INSERT INTO `permission` VALUES (18,'usuario_nuevoUsuario','nuevo usuario');
INSERT INTO `permission` VALUES (19,'teacher_show','ver docentes');
INSERT INTO `permission` VALUES (20,'administrator_index','acceso administrador');
INSERT INTO `permission` VALUES (21,'preceptor_index','preceptor');
INSERT INTO `permission` VALUES (22,'teacher_index','docente');
INSERT INTO `permission` VALUES (23,'user_new','crear usuario');
INSERT INTO `permission` VALUES (24,'user_profile','perfil de usuario');
INSERT INTO `permission` VALUES (25,'user_update_info','actualizar usuario');
INSERT INTO `permission` VALUES (26,'send_email','enviar mail');
INSERT INTO `permission` VALUES (27,'confirm_email','confirmar mail');
INSERT INTO `permission` VALUES (28,'activate','activar');
INSERT INTO `permission` VALUES (29,'search_users','buscar usuarios');
INSERT INTO `permission` VALUES (30,'search_administrators','buscar administradores');
INSERT INTO `permission` VALUES (31,'search_preceptors','buscar preceptores');
INSERT INTO `permission` VALUES (32,'search_teachers','buscar docentes');
INSERT INTO `permission` VALUES (33,'student_index','acceso alumnos');
INSERT INTO `permission` VALUES (34,'instrument_index','acceso instrumentos');
INSERT INTO `permission` VALUES (35,'class_index','acceso clase');
INSERT INTO `permission` VALUES (36,'configuration_show_permissions','mostrar permisos');
INSERT INTO `permission` VALUES (37,'configuration_show_roles_permissions','mostrar roles con permisos');
INSERT INTO `permission` VALUES (38,'configuration_create_permission','crear permiso');
INSERT INTO `permission` VALUES (39,'configuration_update_permission','actualizar permiso');
INSERT INTO `permission` VALUES (40,'configuration_delete_permission','eliminar permiso');
INSERT INTO `permission` VALUES (41,'configuration_assign_permissions','asignar permiso');
INSERT INTO `permission` VALUES (42,'user_delete','eliminar usuario');
INSERT INTO `permission` VALUES (43,'user_block','bloque usuario');
INSERT INTO `permission` VALUES (44,'user_update_role','actualizar rol');
INSERT INTO `permission` VALUES (45,'change_password','cambiar contraseña');
INSERT INTO `permission` VALUES (46,'configuration_public_info_index','acceso informacion publica');
INSERT INTO `permission` VALUES (47,'configuration_pagination_index','acceso paginacion');
INSERT INTO `permission` VALUES (48,'configuration_maintenance_mode_index','acceso mantenimiento');
INSERT INTO `permission` VALUES (49,'auth_authorize','autorizar');
INSERT INTO `permission` VALUES (50,'user_exist','chequear usuario');
INSERT INTO `permission` VALUES (51,'user_get_block','acceder bloque usuarrio');
INSERT INTO `permission` VALUES (52,'user_get_roles','acceder a los roles de un usuario');
INSERT INTO `permission` VALUES (53,'enrollment_index','inscripciones');
INSERT INTO `permission` VALUES (54,'teacher_not_enrolled','docentes no inscriptos');
INSERT INTO `permission` VALUES (55,'enrollment_search','buscar inscripcion');
INSERT INTO `permission` VALUES (56,'enrollment_create_teacher','agregar docente a inscripccion');
INSERT INTO `permission` VALUES (57,'enrollment_create_student','agregar alumno a inscripccion');
INSERT INTO `permission` VALUES (58,'enrollment_student_index','acceder inscripccion alumno');
INSERT INTO `permission` VALUES (59,'enrollment_teacher_index','acceder inscripcion docente');
INSERT INTO `permission` VALUES (60,'student_not_enrolled','chequear alumno no inscripto');
INSERT INTO `permission` VALUES (61,'student_index_small','acceso alumnos');
INSERT INTO `permission` VALUES (62,'student_new','crear nuevo alumno');
INSERT INTO `permission` VALUES (63,'student_exist','chequear alumno existe');
INSERT INTO `permission` VALUES (64,'student_update_info','actualizar la informacion del alumno');
INSERT INTO `permission` VALUES (65,'student_profile','perfil del alumno');
INSERT INTO `permission` VALUES (66,'student_delete','eliminar alumno');
INSERT INTO `permission` VALUES (67,'guardian_index','acceso tutor');
INSERT INTO `permission` VALUES (68,'guardian_update_info','actualizar informacion tutor');
INSERT INTO `permission` VALUES (69,'guardian_delete','eliminar tutor');
INSERT INTO `permission` VALUES (70,'teacher_profile','acceder al perfil docente');
INSERT INTO `permission` VALUES (71,'teacher_update_info','actualizar informacion del docente');
INSERT INTO `permission` VALUES (72,'teacher_delete','eliminar docente');
INSERT INTO `permission` VALUES (73,'get_neighborhood','acceder barrio');
INSERT INTO `permission` VALUES (74,'get_school','acceder escuela');
INSERT INTO `permission` VALUES (75,'get_level','acceder nivel');
INSERT INTO `permission` VALUES (76,'get_gender','acceder genero');
INSERT INTO `permission` VALUES (77,'guardian_new','nuevo tutor');
INSERT INTO `permission` VALUES (78,'guardian_profile','perfil del tutor');
INSERT INTO `permission` VALUES (79,'guardian_exist','chequear existe tutor');
INSERT INTO `permission` VALUES (80,'enrolled_student_index','acceder alumnos inscriptos');
INSERT INTO `permission` VALUES (81,'enrolled_teacher_index','acceder docentes inscriptos');
INSERT INTO `permission` VALUES (82,'teacher_detail','acceder detalles docente');
INSERT INTO `permission` VALUES (83,'student_detail','acceder detalles alumno');
INSERT INTO `permission` VALUES (84,'workshop_index','acceder talleres');
INSERT INTO `permission` VALUES (85,'new_workshop','crear taller');
INSERT INTO `permission` VALUES (86,'workshop_delete','eliminar taller');
INSERT INTO `permission` VALUES (87,'workshop_edit','actualizar taller');
INSERT INTO `permission` VALUES (88,'workshop_finalEdit','actualizar taller plus');
INSERT INTO `permission` VALUES (89,'about_getInfo','acceder a info about');
INSERT INTO `permission` VALUES (90,'edit_workshop','actualizar taller plus');
INSERT INTO `permission` VALUES (91,'roles_index','acceder roles');
INSERT INTO `permission` VALUES (92,'period_index','acceder ciclos lectivos');
INSERT INTO `permission` VALUES (93,'period_create','crear ciclos lectivos');
INSERT INTO `permission` VALUES (94,'period_update','actualizar ciclos lectivos');
INSERT INTO `permission` VALUES (95,'period_delete','eliminar ciclos lectivos');
INSERT INTO `permission` VALUES (96,'period_assign','panel taller - ciclo lectivo');
INSERT INTO `permission` VALUES (97,'period_enrollment','acceder inscripciones del ciclo lectivo');
INSERT INTO `permission` VALUES (98,'period_workshop','acceder talleres del ciclo lectivo');
INSERT INTO `permission` VALUES (99,'period_assign_workshop','asignar taller a ciclo lectivo');
INSERT INTO `permission` VALUES (100,'teacher_enrolled','mostrar docentes inscriptos');
INSERT INTO `permission` VALUES (101,'student_enrolled','mostrar alumnos inscriptos');
INSERT INTO `permission` VALUES (102,'enrollment_show','mostrar inscripcion');
INSERT INTO `permission` VALUES (103,'enrollment_detail','acceder a informacion inscripcion');
INSERT INTO `permission` VALUES (104,'center_index','obtener listado de nucleos');
INSERT INTO `permission` VALUES (105,'weekday_index','obtener listado de dias de la semana');
INSERT INTO `permission` VALUES (106,'center_update_info','actualizar informacion nucleo');
INSERT INTO `permission` VALUES (107,'center_delete','eliminar nucleo');
INSERT INTO `permission` VALUES (108,'center_profile','perfil del nucleo');
INSERT INTO `permission` VALUES (109,'center_exist','chequear existe nucleo');
INSERT INTO `permission` VALUES (110,'center_new','nuevo nucleo');
INSERT INTO `permission` VALUES (111,'event_index','obtener listado de eventos');
INSERT INTO `permission` VALUES (112,'event_new','nuevo evento');
INSERT INTO `permission` VALUES (113,'event_delete','eliminar evento');
INSERT INTO `permission` VALUES (114,'event_create','crear evento');
INSERT INTO `permission` VALUES (115,'venue_index','acceder venues');
INSERT INTO `permission` VALUES (116,'event_detail','acceder a un evento');
INSERT INTO `permission` VALUES (117,'event_show','detalle de un evento');
INSERT INTO `permission` VALUES (118,'level_cant','cant alumnos por nivel');
INSERT INTO `permission` VALUES (119,'user_role_cant','cant usuarios por rol');
INSERT INTO `permission` VALUES (120,'user_confirm_cant','cant usuarios confirmados');
INSERT INTO `permission` VALUES (121,'user_bloqued_cant','cant usuarios bloqueados');
INSERT INTO `permission` VALUES (122,'venue_new','nuevo venue');
INSERT INTO `permission` VALUES (123,'venue_create','crear venue');
INSERT INTO `permission` VALUES (124,'course_index','ver cursos');
INSERT INTO `permission` VALUES (125,'course_teacher_index','ver mis cursos');
INSERT INTO `permission` VALUES (126,'course_new','crear curso');
INSERT INTO `permission` VALUES (127,'instrument_profile','perfil de instrumento');
INSERT INTO `permission` VALUES (128,'instrument_exist','chequeo existencia instrumento');
INSERT INTO `permission` VALUES (129,'instrument_type_index','ver tipos de instrumentos');
INSERT INTO `permission` VALUES (130,'instrument_update_info','actualizar instrumento');
INSERT INTO `permission` VALUES (131,'instrument_delete','eliminar instrumento');
INSERT INTO `permission` VALUES (132,'instrument_new','crear instrumento');
INSERT INTO `permission` VALUES (133,'event_invite','invitar a evento');
INSERT INTO `permission` VALUES (134,'attendee_event','obtener listado de invitados');
INSERT INTO `permission` VALUES (135,'period_new','crear ciclo');
INSERT INTO `permission` VALUES (136,'period_profile','perfil de ciclo');
INSERT INTO `permission` VALUES (137,'period_exist','chequeo existencia ciclo');
INSERT INTO `permission` VALUES (138,'period_update_info','actualizar ciclo');
INSERT INTO `permission` VALUES (139,'instrument_type_new','crear tipo de instrumento');
INSERT INTO `permission` VALUES (140,'instrument_type_delete','eliminar tipo instrumento');
INSERT INTO `permission` VALUES (141,'course_profile','perfil de curso');
INSERT INTO `permission` VALUES (142,'course_exist','chequeo existencia curso');
INSERT INTO `permission` VALUES (143,'course_update_info','actualizar curso');
INSERT INTO `permission` VALUES (144,'course_delete','eliminar curso');
INSERT INTO `permission` VALUES (145,'attendance_index','ver pasar asistencia');
INSERT INTO `permission` VALUES (146,'attendance_new','pasar asistencia');
INSERT INTO `permission` VALUES (147,'enrollment_exist_teacher','chequeo existencia docente en el curso');
INSERT INTO `permission` VALUES (148,'student_enrolled_attendance','alumnos y asistencias');
INSERT INTO `permission` VALUES (149,'loan_index','ver prestamos');
INSERT INTO `permission` VALUES (150,'new_loan','nuevo prestamos');


/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (1,1);
INSERT INTO `role_permission` VALUES (1,2);
INSERT INTO `role_permission` VALUES (1,3);
INSERT INTO `role_permission` VALUES (1,4);
INSERT INTO `role_permission` VALUES (1,5);
INSERT INTO `role_permission` VALUES (1,6);
INSERT INTO `role_permission` VALUES (1,7);
INSERT INTO `role_permission` VALUES (1,8);
INSERT INTO `role_permission` VALUES (1,9);
INSERT INTO `role_permission` VALUES (1,10);
INSERT INTO `role_permission` VALUES (1,11);
INSERT INTO `role_permission` VALUES (1,12);
INSERT INTO `role_permission` VALUES (1,13);
INSERT INTO `role_permission` VALUES (1,14);
INSERT INTO `role_permission` VALUES (1,15);
INSERT INTO `role_permission` VALUES (1,16);
INSERT INTO `role_permission` VALUES (1,17);
INSERT INTO `role_permission` VALUES (1,18);
INSERT INTO `role_permission` VALUES (1,19);
INSERT INTO `role_permission` VALUES (1,20);
INSERT INTO `role_permission` VALUES (1,21);
INSERT INTO `role_permission` VALUES (1,22);
INSERT INTO `role_permission` VALUES (1,23);
INSERT INTO `role_permission` VALUES (1,24);
INSERT INTO `role_permission` VALUES (1,25);
INSERT INTO `role_permission` VALUES (1,26);
INSERT INTO `role_permission` VALUES (1,27);
INSERT INTO `role_permission` VALUES (1,28);
INSERT INTO `role_permission` VALUES (1,29);
INSERT INTO `role_permission` VALUES (1,30);
INSERT INTO `role_permission` VALUES (1,31);
INSERT INTO `role_permission` VALUES (1,32);
INSERT INTO `role_permission` VALUES (1,33);
INSERT INTO `role_permission` VALUES (1,34);
INSERT INTO `role_permission` VALUES (1,35);
INSERT INTO `role_permission` VALUES (1,36);
INSERT INTO `role_permission` VALUES (1,37);
INSERT INTO `role_permission` VALUES (1,38);
INSERT INTO `role_permission` VALUES (1,39);
INSERT INTO `role_permission` VALUES (1,40);
INSERT INTO `role_permission` VALUES (1,41);
INSERT INTO `role_permission` VALUES (1,42);
INSERT INTO `role_permission` VALUES (1,43);
INSERT INTO `role_permission` VALUES (1,44);
INSERT INTO `role_permission` VALUES (1,45);
INSERT INTO `role_permission` VALUES (1,46);
INSERT INTO `role_permission` VALUES (1,47);
INSERT INTO `role_permission` VALUES (1,48);
INSERT INTO `role_permission` VALUES (1,49);
INSERT INTO `role_permission` VALUES (1,50);
INSERT INTO `role_permission` VALUES (1,51);
INSERT INTO `role_permission` VALUES (1,52);
INSERT INTO `role_permission` VALUES (1,53);
INSERT INTO `role_permission` VALUES (1,54);
INSERT INTO `role_permission` VALUES (1,55);
INSERT INTO `role_permission` VALUES (1,56);
INSERT INTO `role_permission` VALUES (1,57);
INSERT INTO `role_permission` VALUES (1,58);
INSERT INTO `role_permission` VALUES (1,59);
INSERT INTO `role_permission` VALUES (1,60);
INSERT INTO `role_permission` VALUES (1,61);
INSERT INTO `role_permission` VALUES (1,62);
INSERT INTO `role_permission` VALUES (1,63);
INSERT INTO `role_permission` VALUES (1,64);
INSERT INTO `role_permission` VALUES (1,65);
INSERT INTO `role_permission` VALUES (1,66);
INSERT INTO `role_permission` VALUES (1,67);
INSERT INTO `role_permission` VALUES (1,68);
INSERT INTO `role_permission` VALUES (1,69);
INSERT INTO `role_permission` VALUES (1,70);
INSERT INTO `role_permission` VALUES (1,71);
INSERT INTO `role_permission` VALUES (1,72);
INSERT INTO `role_permission` VALUES (1,73);
INSERT INTO `role_permission` VALUES (1,74);
INSERT INTO `role_permission` VALUES (1,75);
INSERT INTO `role_permission` VALUES (1,76);
INSERT INTO `role_permission` VALUES (1,77);
INSERT INTO `role_permission` VALUES (1,78);
INSERT INTO `role_permission` VALUES (1,79);
INSERT INTO `role_permission` VALUES (1,80);
INSERT INTO `role_permission` VALUES (1,81);
INSERT INTO `role_permission` VALUES (1,82);
INSERT INTO `role_permission` VALUES (1,83);
INSERT INTO `role_permission` VALUES (1,84);
INSERT INTO `role_permission` VALUES (1,85);
INSERT INTO `role_permission` VALUES (1,86);
INSERT INTO `role_permission` VALUES (1,87);
INSERT INTO `role_permission` VALUES (1,88);
INSERT INTO `role_permission` VALUES (1,89);
INSERT INTO `role_permission` VALUES (1,90);
INSERT INTO `role_permission` VALUES (1,91);
INSERT INTO `role_permission` VALUES (1,92);
INSERT INTO `role_permission` VALUES (1,93);
INSERT INTO `role_permission` VALUES (1,94);
INSERT INTO `role_permission` VALUES (1,95);
INSERT INTO `role_permission` VALUES (1,96);
INSERT INTO `role_permission` VALUES (1,97);
INSERT INTO `role_permission` VALUES (1,98);
INSERT INTO `role_permission` VALUES (1,99);
INSERT INTO `role_permission` VALUES (1,100);
INSERT INTO `role_permission` VALUES (1,101);
INSERT INTO `role_permission` VALUES (1,102);
INSERT INTO `role_permission` VALUES (1,103);
INSERT INTO `role_permission` VALUES (1,104);
INSERT INTO `role_permission` VALUES (1,105);
INSERT INTO `role_permission` VALUES (1,106);
INSERT INTO `role_permission` VALUES (1,107);
INSERT INTO `role_permission` VALUES (1,108);
INSERT INTO `role_permission` VALUES (1,109);
INSERT INTO `role_permission` VALUES (1,110);
INSERT INTO `role_permission` VALUES (1,111);
INSERT INTO `role_permission` VALUES (1,112);
INSERT INTO `role_permission` VALUES (1,113);
INSERT INTO `role_permission` VALUES (1,114);
INSERT INTO `role_permission` VALUES (1,115);
INSERT INTO `role_permission` VALUES (1,116);
INSERT INTO `role_permission` VALUES (1,117);
INSERT INTO `role_permission` VALUES (1,118);
INSERT INTO `role_permission` VALUES (1,119);
INSERT INTO `role_permission` VALUES (1,120);
INSERT INTO `role_permission` VALUES (1,121);
INSERT INTO `role_permission` VALUES (1,122);
INSERT INTO `role_permission` VALUES (1,123);
INSERT INTO `role_permission` VALUES (1,124);
INSERT INTO `role_permission` VALUES (1,125);
INSERT INTO `role_permission` VALUES (1,126);
INSERT INTO `role_permission` VALUES (1,127);
INSERT INTO `role_permission` VALUES (1,128);
INSERT INTO `role_permission` VALUES (1,129);
INSERT INTO `role_permission` VALUES (1,130);
INSERT INTO `role_permission` VALUES (1,131);
INSERT INTO `role_permission` VALUES (1,132);
INSERT INTO `role_permission` VALUES (1,133);
INSERT INTO `role_permission` VALUES (1,134);
INSERT INTO `role_permission` VALUES (1,135);
INSERT INTO `role_permission` VALUES (1,136);
INSERT INTO `role_permission` VALUES (1,137);
INSERT INTO `role_permission` VALUES (1,138);
INSERT INTO `role_permission` VALUES (1,139);
INSERT INTO `role_permission` VALUES (1,140);
INSERT INTO `role_permission` VALUES (1,141);
INSERT INTO `role_permission` VALUES (1,142);
INSERT INTO `role_permission` VALUES (1,143);
INSERT INTO `role_permission` VALUES (1,144);
INSERT INTO `role_permission` VALUES (1,145);
INSERT INTO `role_permission` VALUES (1,146);
INSERT INTO `role_permission` VALUES (1,147);
INSERT INTO `role_permission` VALUES (1,148);
INSERT INTO `role_permission` VALUES (1,149);
INSERT INTO `role_permission` VALUES (1,150);
INSERT INTO `role_permission` VALUES (2,1);
INSERT INTO `role_permission` VALUES (2,2);
INSERT INTO `role_permission` VALUES (2,3);
INSERT INTO `role_permission` VALUES (2,4);
INSERT INTO `role_permission` VALUES (2,5);
INSERT INTO `role_permission` VALUES (2,12);
INSERT INTO `role_permission` VALUES (2,19);
INSERT INTO `role_permission` VALUES (2,21);
INSERT INTO `role_permission` VALUES (2,22);
INSERT INTO `role_permission` VALUES (2,24);
INSERT INTO `role_permission` VALUES (2,26);
INSERT INTO `role_permission` VALUES (2,27);
INSERT INTO `role_permission` VALUES (2,28);
INSERT INTO `role_permission` VALUES (2,31);
INSERT INTO `role_permission` VALUES (2,32);
INSERT INTO `role_permission` VALUES (2,33);
INSERT INTO `role_permission` VALUES (2,34);
INSERT INTO `role_permission` VALUES (2,35);
INSERT INTO `role_permission` VALUES (2,36);
INSERT INTO `role_permission` VALUES (2,49);
INSERT INTO `role_permission` VALUES (3,1);
INSERT INTO `role_permission` VALUES (3,2);
INSERT INTO `role_permission` VALUES (3,3);
INSERT INTO `role_permission` VALUES (3,4);
INSERT INTO `role_permission` VALUES (3,5);
INSERT INTO `role_permission` VALUES (3,12);
INSERT INTO `role_permission` VALUES (3,19);
INSERT INTO `role_permission` VALUES (3,22);
INSERT INTO `role_permission` VALUES (3,24);
INSERT INTO `role_permission` VALUES (3,26);
INSERT INTO `role_permission` VALUES (3,27);
INSERT INTO `role_permission` VALUES (3,28);
INSERT INTO `role_permission` VALUES (3,32);
INSERT INTO `role_permission` VALUES (3,33);
INSERT INTO `role_permission` VALUES (3,34);
INSERT INTO `role_permission` VALUES (3,35);
INSERT INTO `role_permission` VALUES (3,36);
INSERT INTO `role_permission` VALUES (3,47);
INSERT INTO `role_permission` VALUES (3,49);
INSERT INTO `role_permission` VALUES (3,52);
INSERT INTO `role_permission` VALUES (3,53);
INSERT INTO `role_permission` VALUES (3,54);
INSERT INTO `role_permission` VALUES (3,55);
INSERT INTO `role_permission` VALUES (3,56);
INSERT INTO `role_permission` VALUES (3,57);
INSERT INTO `role_permission` VALUES (3,58);
INSERT INTO `role_permission` VALUES (3,59);
INSERT INTO `role_permission` VALUES (3,60);
INSERT INTO `role_permission` VALUES (3,61);
INSERT INTO `role_permission` VALUES (3,80);
INSERT INTO `role_permission` VALUES (3,81);
INSERT INTO `role_permission` VALUES (3,100);
INSERT INTO `role_permission` VALUES (3,101);
INSERT INTO `role_permission` VALUES (3,102);
INSERT INTO `role_permission` VALUES (3,103);
INSERT INTO `role_permission` VALUES (3,118);
INSERT INTO `role_permission` VALUES (3,125);
INSERT INTO `role_permission` VALUES (3,127);
INSERT INTO `role_permission` VALUES (3,128);
INSERT INTO `role_permission` VALUES (3,129);
INSERT INTO `role_permission` VALUES (3,141);
INSERT INTO `role_permission` VALUES (3,142);
INSERT INTO `role_permission` VALUES (3,145);
INSERT INTO `role_permission` VALUES (3,146);
INSERT INTO `role_permission` VALUES (3,147);
INSERT INTO `role_permission` VALUES (3,148);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1);
INSERT INTO `user_role` VALUES (1,2);
INSERT INTO `user_role` VALUES (1,3);
INSERT INTO `user_role` VALUES (2,1);
INSERT INTO `user_role` VALUES (3,2);
INSERT INTO `user_role` VALUES (4,3);
INSERT INTO `user_role` VALUES (5,1);
INSERT INTO `user_role` VALUES (5,2);
INSERT INTO `user_role` VALUES (6,1);
INSERT INTO `user_role` VALUES (6,3);
INSERT INTO `user_role` VALUES (7,2);
INSERT INTO `user_role` VALUES (7,3);
INSERT INTO `user_role` VALUES (8,1);
INSERT INTO `user_role` VALUES (8,2);
INSERT INTO `user_role` VALUES (8,3);
INSERT INTO `user_role` VALUES (9,1);
INSERT INTO `user_role` VALUES (10,2);
INSERT INTO `user_role` VALUES (11,3);
INSERT INTO `user_role` VALUES (12,3);
INSERT INTO `user_role` VALUES (13,3);
INSERT INTO `user_role` VALUES (14,3);
INSERT INTO `user_role` VALUES (15,3);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `level`
--

LOCK TABLES `level` WRITE;
/*!40000 ALTER TABLE `level` DISABLE KEYS */;
INSERT INTO `level` VALUES (1,'Sin nivel');
INSERT INTO `level` VALUES (2,'Rojo');
INSERT INTO `level` VALUES (3,'Verde');
INSERT INTO `level` VALUES (4,'Amarillo');
INSERT INTO `level` VALUES (5,'Naranja');
INSERT INTO `level` VALUES (6,'Rosa');
INSERT INTO `level` VALUES (7,'Azul');
/*!40000 ALTER TABLE `level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,'Masculino');
INSERT INTO `gender` VALUES (2,'Femenino');
INSERT INTO `gender` VALUES (3,'Otro');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `school`
--

LOCK TABLES `school` WRITE;
/*!40000 ALTER TABLE `school` DISABLE KEYS */;
INSERT INTO `school` VALUES (1,'Bellas Artes','Calle 1 e/ 2 y 3','221-411-2233');
INSERT INTO `school` VALUES (2,'San Luis','Calle 4 e/ 5 y 6','221-444-5566');
INSERT INTO `school` VALUES (3,'Anexa','Calle 7 e/ 8 y 9','221-477-8899');
INSERT INTO `school` VALUES (4,'Albert Thomas','Calle 10 e/ 11 y 12','221-400-1122');
INSERT INTO `school` VALUES (5,'Nacional','Calle 12 e/ 13 y 14','221-433-4455');
INSERT INTO `school` VALUES (6,'Liceo','Calle 15 e/ 16 y 17','221-444-6677');
INSERT INTO `school` VALUES (7,'Estrada','Calle 18 e/ 19 y 20','221-455-8899');
INSERT INTO `school` VALUES (8,'Universitas','Calle 21 e/ 22 y 23','221-466-1122');
INSERT INTO `school` VALUES (9,'Italiana','Calle 24 e/ 25 y 26','221-477-1122');
/*!40000 ALTER TABLE `school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `neighborhood`
--

LOCK TABLES `neighborhood` WRITE;
/*!40000 ALTER TABLE `neighborhood` DISABLE KEYS */;
INSERT INTO `neighborhood` VALUES (1, 'Barrio Nautico');
INSERT INTO `neighborhood` VALUES (2, 'Barrio Obrero');
INSERT INTO `neighborhood` VALUES (3, 'Berisso');
INSERT INTO `neighborhood` VALUES (4, 'Barrio Solidaridad');
INSERT INTO `neighborhood` VALUES (5, 'Barrio Bco. Pcia.');
INSERT INTO `neighborhood` VALUES (6, 'Barrio J.B. Justo');
INSERT INTO `neighborhood` VALUES (7, 'El Carmen');
INSERT INTO `neighborhood` VALUES (8, 'El Labrador');
INSERT INTO `neighborhood` VALUES (9, 'Ensenada');
INSERT INTO `neighborhood` VALUES (10, 'La Hermosura');
INSERT INTO `neighborhood` VALUES (11, 'La PLata');
INSERT INTO `neighborhood` VALUES (12, 'Los Talas');
INSERT INTO `neighborhood` VALUES (13, 'Ringuelet');
INSERT INTO `neighborhood` VALUES (14, 'Tolosa');
INSERT INTO `neighborhood` VALUES (15, 'Villa Alba');
INSERT INTO `neighborhood` VALUES (16, 'Villa Arguello');
INSERT INTO `neighborhood` VALUES (17, 'Villa B. C');
INSERT INTO `neighborhood` VALUES (18, 'Villa Elvira');
INSERT INTO `neighborhood` VALUES (19, 'Villa Nueva');
INSERT INTO `neighborhood` VALUES (20, 'Villa Paula');
INSERT INTO `neighborhood` VALUES (21, 'Villa Progreso');
INSERT INTO `neighborhood` VALUES (22, 'Villa San Carlos');
INSERT INTO `neighborhood` VALUES (23, 'Villa Zula');
/*!40000 ALTER TABLE `neighborhood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Pablo','Tingler','10123456','Calle A e/ B y C',5,1,'2000-01-1',1,'0123456789',1,1,1,1);
INSERT INTO `student` VALUES (2,'Bronson','Axtell','11789123','Calle D e/ E y F',5,1,'2001-02-2',2,'1123456789',1,2,2,1);
INSERT INTO `student` VALUES (3,'Colten','Bury','22456789','Calle G e/ H y I',6,2,'2002-03-3',3,'2123456789',2,3,3,2);
INSERT INTO `student` VALUES (4,'Caden','Ackerman','33789123','Calle J e/ K y L',6,2,'2003-04-4',4,'3123456789',2,4,1,3);
INSERT INTO `student` VALUES (5,'Lorena','Hail','44123456','Calle M e/ N y O',6,3,'2004-05-5',5,'4123456789',2,4,1,3);
INSERT INTO `student` VALUES (6,'Derek','Clayton','55456789','Calle P e/ Q y R',4,4,'2005-06-6',1,'5123456789',3,1,1,1);
INSERT INTO `student` VALUES (7,'Jenson','Sutherland','66789123','Calle S e/ T y U',3,5,'2006-07-7',1,'6123456789',2,1,2,1);
INSERT INTO `student` VALUES (8,'Jannah','Pearson','77123456','Calle V e/ W y X',2,6,'2007-08-8',2,'7123456789',2,1,3,2);
INSERT INTO `student` VALUES (9,'Gerrard','Weaver','88456789','Calle Y e/ Z y AA',1,6,'2008-09-9',3,'8123456789',3,2,3,2);
INSERT INTO `student` VALUES (10,'Atlas','Jacobson','99123456','Calle BB e/ CC y DD',2,7,'2009-10-10',3,'9123456789',1,3,2,2);
INSERT INTO `student` VALUES (11,'Samantha','Cullen','10789123','Calle EE e/ FF y GG',3,6,'2010-11-11',3,'1023456789',1,1,2,1);
INSERT INTO `student` VALUES (12,'Justin','Fischer','12123456','Calle HH e/ II y JJ',3,3,'2011-12-12',6,'1113456789',2,2,1,3);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `center`
--

LOCK TABLES `center` WRITE;
/*!40000 ALTER TABLE `center` DISABLE KEYS */;
INSERT INTO `center` VALUES (1,'EP 6','8 y 158','Berisso','221-411-2233','-34.865844','-57.903189');
INSERT INTO `center` VALUES (2,'EP 7','151 8 y 9','Berisso','221-444-5566','-34.865844','-57.903189');
INSERT INTO `center` VALUES (3,'EP 8','63 Y 125','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (4,'EP 14','96 Y 126','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (5,'EP 17','164 Y 26','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (6,'EP 20','RUTA 11 KM 13 - PJE LA HERMOSURA','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (7,'EP 22','32 Y 173','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (8,'EP 25','126 E 29 Y 30','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (9,'JARDIN 904','164 E 30 Y 31','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (10,'EEE 501','PASCUAL RUBERTO E 168 Y 169','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (11,'CIC','169 y 33','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (12,'PARROQUIA SAN MIGUEL ARCANGEL','63 y 124','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (13,'CENTRO CULTURAL PAPA FRANCISCO','44 y 126','Berisso','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (14,'CLUB ESPAÑOL','6 e 53 y 54','La Plata','221-477-8899','-34.865844','-57.903189');
INSERT INTO `center` VALUES (15,'TEATRO ARGENTINO','53 e 9 y 10','La Plata','221-477-8899','-34.865844','-57.903189');


/*!40000 ALTER TABLE `center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_type`
--

LOCK TABLES `item_type` WRITE;
/*!40000 ALTER TABLE `item_type` DISABLE KEYS */;
INSERT INTO `item_type` VALUES (1,'Piano');
INSERT INTO `item_type` VALUES (2,'Violin');
INSERT INTO `item_type` VALUES (3,'Flauta');
INSERT INTO `item_type` VALUES (4,'Contrabajo');
INSERT INTO `item_type` VALUES (5,'Violoncello');
INSERT INTO `item_type` VALUES (6,'Tuba');
INSERT INTO `item_type` VALUES (7,'Trombon');
INSERT INTO `item_type` VALUES (8,'Corno');
INSERT INTO `item_type` VALUES (9,'Fagot');
INSERT INTO `item_type` VALUES (10,'Oboe');
INSERT INTO `item_type` VALUES (11,'Clarinete');

/*!40000 ALTER TABLE `item_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workshop`
--

LOCK TABLES `workshop` WRITE;
/*!40000 ALTER TABLE `workshop` DISABLE KEYS */;
INSERT INTO `workshop` VALUES (1,'piano');
INSERT INTO `workshop` VALUES (2,'violin');
INSERT INTO `workshop` VALUES (3,'flauta');
INSERT INTO `workshop` VALUES (4,'contrabajo');
INSERT INTO `workshop` VALUES (5,'violoncello');
INSERT INTO `workshop` VALUES (6,'trombon');
INSERT INTO `workshop` VALUES (7,'trompeta');
INSERT INTO `workshop` VALUES (8,'clarinete');
INSERT INTO `workshop` VALUES (9,'coro');
INSERT INTO `workshop` VALUES (10,'lenguaje');
INSERT INTO `workshop` VALUES (11,'orquesta inicial');
INSERT INTO `workshop` VALUES (12,'orquesta pre-juvenil');
INSERT INTO `workshop` VALUES (13,'orquesta juvenil');
INSERT INTO `workshop` VALUES (14,'orquesta pre-camerata');
INSERT INTO `workshop` VALUES (15,'camerata');
INSERT INTO `workshop` VALUES (16,'banda de vientos');
INSERT INTO `workshop` VALUES (17,'quinteto de vientos');
/*!40000 ALTER TABLE `workshop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `period`
--

LOCK TABLES `period` WRITE;
/*!40000 ALTER TABLE `period` DISABLE KEYS */;
INSERT INTO `period` VALUES (1,'2018-01-01','2018-06-30','1');
INSERT INTO `period` VALUES (2,'2018-07-01','2018-12-31','2');
INSERT INTO `period` VALUES (3,'2019-01-01','2019-06-30','1');
INSERT INTO `period` VALUES (4,'2019-07-01','2019-12-31','2');
/*!40000 ALTER TABLE `period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `weekday`
--

LOCK TABLES `weekday` WRITE;
/*!40000 ALTER TABLE `weekday` DISABLE KEYS */;
INSERT INTO `weekday` VALUES (1,'lunes');
INSERT INTO `weekday` VALUES (2,'martes');
INSERT INTO `weekday` VALUES (3,'miercoles');
INSERT INTO `weekday` VALUES (4,'jueves');
INSERT INTO `weekday` VALUES (5,'viernes');
INSERT INTO `weekday` VALUES (6,'sabado');
INSERT INTO `weekday` VALUES (7,'domingo');
/*!40000 ALTER TABLE `weekday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `enrollment`
--

LOCK TABLES `enrollment` WRITE;
/*!40000 ALTER TABLE `enrollment` DISABLE KEYS */;
INSERT INTO `enrollment` VALUES (1,1,1,1,1,"15:30");
INSERT INTO `enrollment` VALUES (2,1,2,1,1,"14:00");
INSERT INTO `enrollment` VALUES (3,2,1,1,2,"13:00");
INSERT INTO `enrollment` VALUES (4,3,2,1,3,"10:15");
INSERT INTO `enrollment` VALUES (5,4,3,2,1,"09:45");
INSERT INTO `enrollment` VALUES (6,5,3,2,2,"12:00");
INSERT INTO `enrollment` VALUES (7,6,3,2,3,"12:00");
INSERT INTO `enrollment` VALUES (8,5,1,1,1,"18:15");
INSERT INTO `enrollment` VALUES (9,7,2,1,4,"17:00");
/*!40000 ALTER TABLE `enrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `inventory_item`
--

LOCK TABLES `inventory_item` WRITE;
/*!40000 ALTER TABLE `inventory_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_log`
--

LOCK TABLES `item_log` WRITE;
/*!40000 ALTER TABLE `item_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `loan`
--

LOCK TABLES `loan` WRITE;
/*!40000 ALTER TABLE `loan` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan` ENABLE KEYS */;
UNLOCK TABLES;

-- Dump completed on 2019-10-28 4:25:31
