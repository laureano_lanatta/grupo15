-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema grupo15
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema grupo15
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `grupo15` DEFAULT CHARACTER SET utf8 ;
USE `grupo15` ;

-- -----------------------------------------------------
-- Table `grupo15`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) GENERATED ALWAYS AS (SUBSTRING(email, 1, LOCATE('@', email) - 1)) VIRTUAL,
  `password` VARCHAR(100) NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL,
  `bloqued` VARCHAR(45) NOT NULL DEFAULT 'no',
  `confirmed` VARCHAR(45) NOT NULL DEFAULT 'no',
  `id_city` INT NULL,
  `phone_number` BIGINT(20) UNSIGNED NULL,
  `address` VARCHAR(45) NULL,
  `birthday` DATE NULL,
  `document_number` VARCHAR(45) NULL,
  `id_document_type` INT NULL,
  `photo` varchar(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`user_role` (
  `id_user` INT NOT NULL,
  `id_role` INT NOT NULL,
  PRIMARY KEY (`id_user`, `id_role`),
  INDEX `idrole_idx` (`id_role` ASC),
  CONSTRAINT `fk_user_role_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `grupo15`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_role_role`
    FOREIGN KEY (`id_role`)
    REFERENCES `grupo15`.`role` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`permission` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `public_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`role_permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`role_permission` (
  `id_role` INT NOT NULL,
  `id_permission` INT NOT NULL,
  PRIMARY KEY (`id_role`, `id_permission`),
  INDEX `idpermission_idx` (`id_permission` ASC),
  CONSTRAINT `fk_role_permission_role`
    FOREIGN KEY (`id_role`)
    REFERENCES `grupo15`.`role` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_permission_permission`
    FOREIGN KEY (`id_permission`)
    REFERENCES `grupo15`.`permission` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`level`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`level` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`neighborhood`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`neighborhood` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`school`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`school` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL,
  `telephone` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`gender`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`gender` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`student` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `phone_number` BIGINT(20) UNSIGNED NULL,
  `address` VARCHAR(45) NOT NULL,
  `id_level` INT NOT NULL,
  `id_city` INT NOT NULL,
  `birthday` DATE NOT NULL,
  `id_birthplace` INT NULL,
  `document_number` VARCHAR(45) NOT NULL,
  `id_neighborhood` INT NOT NULL,
  `id_document_type` INT NOT NULL,
  `id_school` INT NOT NULL,
  `id_gender` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_student_level_idx` (`id_level` ASC),
  INDEX `fk_student_neighborhood_idx` (`id_neighborhood` ASC),
  INDEX `fk_student_school_idx` (`id_school` ASC),
  INDEX `fk_student_gender_idx` (`id_gender` ASC),
  CONSTRAINT `fk_student_level`
    FOREIGN KEY (`id_level`)
    REFERENCES `grupo15`.`level` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_neighborhood`
    FOREIGN KEY (`id_neighborhood`)
    REFERENCES `grupo15`.`neighborhood` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_school`
    FOREIGN KEY (`id_school`)
    REFERENCES `grupo15`.`school` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_gender`
    FOREIGN KEY (`id_gender`)
    REFERENCES `grupo15`.`gender` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`center`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`center` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `phone_number` VARCHAR(45) NULL,
  `lat` VARCHAR(45) NULL,
  `lng` VARCHAR(45) NULL,

  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`workshop`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`workshop` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`period`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`period` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `start` DATE NOT NULL,
  `end` DATE NOT NULL,
  `semester` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`weekday`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`weekday` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`enrollment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`enrollment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_workshop` INT NOT NULL,
  `id_period` INT NOT NULL,
  `id_center` INT NOT NULL,
  `id_weekday` INT NOT NULL,
  `hour` TIME NOT NULL,
  INDEX `fk_enrollment_workshop_idx` (`id_workshop` ASC),
  INDEX `fk_enrollment_period_idx` (`id_period` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_enrollment_center_idx` (`id_center` ASC),
  INDEX `fk_enrollment_weekday_idx` (`id_weekday` ASC),
  CONSTRAINT `fk_enrollment_workshop`
    FOREIGN KEY (`id_workshop`)
    REFERENCES `grupo15`.`workshop` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_enrollment_period`
    FOREIGN KEY (`id_period`)
    REFERENCES `grupo15`.`period` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_enrollment_center`
    FOREIGN KEY (`id_center`)
    REFERENCES `grupo15`.`center` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_enrollment_weekday`
    FOREIGN KEY (`id_weekday`)
    REFERENCES `grupo15`.`weekday` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`item_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`item_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`inventory_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`inventory_item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_item_type` INT NOT NULL,
  `name` varchar(45) NOT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `inventory_number` int(11) NOT NULL,
  PRIMARY KEY (`id`,`inventory_number`),
  INDEX `fk_inventory_item_item_type_idx` (`id_item_type` ASC),
  CONSTRAINT `fk_inventory_item_item_type`
    FOREIGN KEY (`id_item_type`)
    REFERENCES `grupo15`.`item_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`loan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`loan` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_student` INT NOT NULL,
  `id_inventory_item` INT NOT NULL,
  `started_at` DATETIME NOT NULL,
  `ended_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_loan_student_idx` (`id_student` ASC),
  INDEX `fk_loan_inventory_item_idx` (`id_inventory_item` ASC),
  CONSTRAINT `fk_loan_student`
    FOREIGN KEY (`id_student`)
    REFERENCES `grupo15`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_loan_inventory_item`
    FOREIGN KEY (`id_inventory_item`)
    REFERENCES `grupo15`.`inventory_item` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`item_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`item_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `id_inventory_item` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_item_log_inventory_item_idx` (`id_inventory_item` ASC),
  CONSTRAINT `fk_item_log_inventory_item`
    FOREIGN KEY (`id_inventory_item`)
    REFERENCES `grupo15`.`inventory_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`app_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`app_config` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`guardian`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`guardian` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `phone_number` BIGINT(20) UNSIGNED NULL,
  `address` VARCHAR(45) NOT NULL,
  `id_city` INT NOT NULL,
  `birthday` DATE NOT NULL,
  `document_number` VARCHAR(45) NOT NULL,
  `id_document_type` INT NOT NULL,
  `id_gender` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_guardian_gender_idx` (`id_gender` ASC),
  CONSTRAINT `fk_guardian_gender`
    FOREIGN KEY (`id_gender`)
    REFERENCES `grupo15`.`gender` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`student_guardian`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`student_guardian` (
  `id_student` INT NOT NULL,
  `id_guardian` INT NOT NULL,
  PRIMARY KEY (`id_student`, `id_guardian`),
  INDEX `fk_student_guardian_guardian_idx` (`id_guardian` ASC),
  CONSTRAINT `fk_student_guardian_student`
    FOREIGN KEY (`id_student`)
    REFERENCES `grupo15`.`student` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_guardian_guardian`
    FOREIGN KEY (`id_guardian`)
    REFERENCES `grupo15`.`guardian` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`student_enrollment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`student_enrollment` (
  `id_student` INT NOT NULL,
  `id_enrollment` INT NOT NULL,
  PRIMARY KEY (`id_student`, `id_enrollment`),
  INDEX `fk_student_enrollment_enrollment_idx` (`id_enrollment` ASC),
  CONSTRAINT `fk_student_enrollment_student`
    FOREIGN KEY (`id_student`)
    REFERENCES `grupo15`.`student` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_enrollment_enrollment`
    FOREIGN KEY (`id_enrollment`)
    REFERENCES `grupo15`.`enrollment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grupo15`.`user_enrollment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`user_enrollment` (
  `id_user` INT NOT NULL,
  `id_enrollment` INT NOT NULL,
  PRIMARY KEY (`id_user`, `id_enrollment`),
  INDEX `fk_user_enrollment_enrollment_idx` (`id_enrollment` ASC),
  CONSTRAINT `fk_user_enrollment_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `grupo15`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_enrollment_enrollment`
    FOREIGN KEY (`id_enrollment`)
    REFERENCES `grupo15`.`enrollment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `grupo15`.`attendance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`attendance` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_student` INT NOT NULL,
  `id_enrollment` INT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_attendance_student_idx` (`id_student` ASC),
  INDEX `fk_attendance_enrollment_idx` (`id_enrollment` ASC),
  CONSTRAINT `fk_attendance_enrollment`
    FOREIGN KEY (`id_enrollment`)
    REFERENCES `grupo15`.`enrollment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attendance_student`
    FOREIGN KEY (`id_student`)
    REFERENCES `grupo15`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `grupo15`.`attendance_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grupo15`.`attendance_user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `id_enrollment` INT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_attendance_user_id_idx` (`id_user` ASC),
  INDEX `fk_attendance_user_enrollment_idx` (`id_enrollment` ASC),
  CONSTRAINT `fk_attendance_user_enrollment`
    FOREIGN KEY (`id_enrollment`)
    REFERENCES `grupo15`.`enrollment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attendance_user_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `grupo15`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
